import { install, list, uninstall, update, updateOS, get } from "lib/libbpm";
const cmds = ['install', 'uninstall', 'update', 'list', 'get', 'upgrade'];
export function autocomplete(data, args) {
    if (args.length === 0 || !cmds.includes(args[0])) {
        return cmds;
    }
    else if (args[0] === 'install') {
        return [];
    }
    else if (args[0] === 'uninstall') {
        let packages = JSON.parse(localStorage.bpmPackages);
        return Object.keys(packages);
    }
    else if (args[0] === 'update') {
        let packages = JSON.parse(localStorage.bpmPackages);
        return Object.keys(packages);
    }
    else if (args[0] === 'list') {
        return [];
    }
    else if (args[0] === 'get') {
        let packages = JSON.parse(localStorage.bpmPackages);
        return Object.keys(packages);
    }
    else {
        return [];
    }
}
export function help(ns) {
    ns.tprint('BPM Help');
    ns.tprint('bpm <cmd> [args...]');
    ns.tprint('Commands');
    ns.tprint('install <uri> = Install BPM package from URL');
    ns.tprint('uninstall <name> = Uninstall BPM package name');
    ns.tprint('update <name> = Update BPM package');
    ns.tprint('upgrade = Upgrade BitBurnerOS');
    ns.tprint('list = Show list of installed packaged');
    ns.tprint('get <name> = Get package details by name');
}
export async function main(ns) {
    if (ns.args[0] === 'install' && ns.args[1]) {
        await install(ns, ns.args[1]);
    }
    else if (ns.args[0] === 'uninstall' && ns.args[1]) {
        await uninstall(ns, ns.args[1]);
    }
    else if (ns.args[0] === 'update' && ns.args[1]) {
        await update(ns, ns.args[1]);
    }
    else if (ns.args[0] === 'upgrade') {
        await updateOS(ns);
    }
    else if (ns.args[0] === 'list') {
        await list(ns);
    }
    else if (ns.args[0] === 'get' && ns.args[1]) {
        await get(ns, ns.args[1]);
    }
    else {
        help(ns);
    }
}
