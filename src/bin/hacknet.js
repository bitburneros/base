import { getHacknetNodes } from "lib/libhacknet";
import { getPlayer } from "lib/libplayer";
const cmds = ['list', 'buy', 'upgrade', 'get', 'hashes'];
const hashCmds = ['upgrade', 'buy'];
export function autocomplete(data, args) {
    if (args.length === 0 || !cmds.includes(args[0])) {
        return cmds;
    }
    else if (args[0] === 'list') {
        return [];
    }
    else if (args[0] === 'buy') {
        return [];
    }
    else if (args[0] === 'upgrade') {
        return [];
    }
    else if (args[0] === 'get') {
        return [];
    }
    else if (args[0] === 'hashes') {
        return hashCmds;
    }
    else {
        return [];
    }
}
export function help(ns) {
    ns.tprint('Hacknet Help');
    ns.tprint('hacknet <cmd> [args...]');
    ns.tprint('Commands');
    ns.tprint('list = List hacknet nodes');
    ns.tprint('buy <amount> = Buy specified or maximum amount of nodes');
    ns.tprint('upgrade [components=clr] [level=max] [node=all] = Upgrade components (c = cores, l=level, r=ram) for specified or all nodes');
    ns.tprint('get <node> = Get package details by name');
    ns.tprint('hashes = Show hash details');
    ns.tprint('hashes buy <upgrade> [levels=max] = Buy hash upgrade specified levels or maximum possible');
}
export async function main(ns) {
    if (ns.args[0] === 'list') {
        const hacknetNodes = getHacknetNodes();
        if (hacknetNodes) {
            hacknetNodes.forEach((node, i) => {
                ns.tprint(`(${i}) ${node.name} Level(${node.level}) Ram(${node.ram}) Cores(${node.cores}) $${ns.nFormat(node.production, '0,0')}`);
            });
        }
    }
    else if (ns.args[0] === 'buy' && ns.args[1]) {
        const player = getPlayer();
        const hacknetNodes = getHacknetNodes();
        const maxNumNodes = ns.hacknet.maxNumNodes();
        if (player && hacknetNodes && hacknetNodes.length < maxNumNodes) {
            // const maxAmount = maxNumNodes - hacknetNodes.length;
            const amount = ns.args[1];
            for (let i = 0; i < amount; i++) {
                const cost = ns.hacknet.getPurchaseNodeCost();
                if (cost > player.money) {
                    ns.tprint(`You need another $${cost - player.money} to purchase next node`);
                    break;
                }
                ns.hacknet.purchaseNode();
            }
            ns.tprint(`Purchased ${amount} of hacknet nodes`);
        }
        else {
            ns.tprint(`ERROR Max Hacknet Nodes Reached!`);
        }
    }
    else if (ns.args[0] === 'upgrade') {
        let components = 'clr';
        let node = 'all';
        let levels = 'max';
        if (ns.args.length === 2) {
            components = ns.args[1];
        }
        else if (ns.args.length === 3) {
            levels = ns.args[2];
        }
        else if (ns.args.length === 4) {
            components = ns.args[1];
            node = ns.args[3];
        }
        const player = getPlayer();
        const hacknetNodes = getHacknetNodes();
        if (player && hacknetNodes) {
            let indexes = [];
            if (node === 'all') {
                for (let i = 0; i < hacknetNodes.length; i++) {
                    indexes.push(i);
                }
            }
            else {
                indexes.push(node);
            }
            indexes.forEach((index) => {
                const hacknetNode = hacknetNodes[index];
                if (components.includes('c')) {
                    let upLevels = levels;
                    if (levels === 'max') {
                        upLevels = 16 - hacknetNode.cores;
                    }
                    const upgradeCost = ns.hacknet.getCoreUpgradeCost(index, upLevels);
                    if (upgradeCost !== Infinity && upgradeCost < player.money) {
                        ns.hacknet.upgradeCore(index, upLevels);
                    }
                }
                if (components.includes('l')) {
                    let upLevels = levels;
                    if (levels === 'max') {
                        upLevels = 200 - hacknetNode.level;
                    }
                    const upgradeCost = ns.hacknet.getLevelUpgradeCost(index, upLevels);
                    if (upgradeCost !== Infinity && upgradeCost < player.money) {
                        ns.hacknet.upgradeLevel(index, upLevels);
                    }
                }
                if (components.includes('r')) {
                    let upLevels = levels;
                    if (levels === 'max') {
                        upLevels = 64 - hacknetNode.level;
                    }
                    const upgradeCost = ns.hacknet.getRamUpgradeCost(index, upLevels);
                    if (upgradeCost !== Infinity && upgradeCost < player.money) {
                        ns.hacknet.upgradeRam(index, upLevels);
                    }
                }
            });
        }
    }
    else if (ns.args[0] === 'get' && ns.args[1] !== undefined) {
        const hacknetNodes = getHacknetNodes();
        if (hacknetNodes) {
            const node = hacknetNodes[ns.args[1]];
            ns.tprint(`Name: ${node.name}`);
            ns.tprint(`Level: ${node.level}`);
            ns.tprint(`Ram: ${node.ram}`);
            ns.tprint(`Ram Used: ${node.ramUsed}`);
            ns.tprint(`Cores: ${node.cores}`);
            ns.tprint(`Cache: ${node.cache}`);
            ns.tprint(`Hash Capacity: ${node.hashCapacity}`);
            ns.tprint(`Production: $${ns.nFormat(node.production, '0,0')} / sec`);
            ns.tprint(`Total Production: $${ns.nFormat(node.production, '0,0')}`);
        }
    }
    else if (ns.args[0] === 'hashes') {
        const player = getPlayer();
        let curHashes = ns.hacknet.numHashes();
        let maxHashes = ns.hacknet.hashCapacity();
        let upgrades = ns.hacknet.getHashUpgrades().map((upgrade) => { return { name: upgrade, level: ns.hacknet.getHashUpgradeLevel(upgrade), cost: ns.hacknet.hashCost(upgrade) }; });
        if (ns.args[1]) {
            if (ns.args[1] === 'buy') {
                let upgrade = upgrades.find((upgd, i) => { return ns.args[2] === i; });
                if (upgrade) {
                    ns.tprint(`Upgrade: ${upgrade.name}`);
                    ns.tprint(`Cost: ${ns.nFormat(upgrade.cost, '0.000a')}, Level: ${upgrade.level}`);
                    let upgradeCount = 0;
                    if (ns.args[3]) {
                        for (let i = 0; ns.args[3] <= i; i++) {
                            if (ns.hacknet.spendHashes(upgrade.name)) {
                                upgradeCount++;
                            }
                            else {
                                break;
                            }
                        }
                    }
                    else {
                        while (ns.hacknet.spendHashes(upgrade.name)) {
                            upgradeCount++;
                        }
                    }
                    ns.tprint(`Upgraded ${upgradeCount} Times!`);
                }
                else {
                    ns.tprint('ERROR: Upgrade not found!');
                }
            }
            else {
                help(ns);
            }
        }
        else {
            ns.tprint(`Money: ${ns.nFormat(player.money, '$0.000a')}`);
            ns.tprint(`Hashes: ${ns.nFormat(curHashes, '0.000a')}/${ns.nFormat(maxHashes, '0.000a')}`);
            upgrades.forEach((upgrade, i) => {
                ns.tprint(`Upgrade (${i}): ${upgrade.name}`);
                ns.tprint(`Cost: ${ns.nFormat(upgrade.cost, '0.000a')}, Level: ${upgrade.level}`);
            });
        }
    }
    else {
        help(ns);
    }
}
