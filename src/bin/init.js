import { getInitScriptsStatus, startInitScripts, stopInitScripts, getBootScriptsStatus, startBootScripts, stopBootScripts } from "lib/libinit";
const cmds = ['start', 'stop', 'status'];
export function autocomplete(data, args) {
    if (args.length === 0 || !cmds.includes(args[0])) {
        return cmds;
    }
    else {
        return [];
    }
}
export function help(ns) {
    ns.tprint('Init Help');
    ns.tprint('init <cmd> [args...]');
    ns.tprint('Commands');
    ns.tprint('start = Start boot and init scripts');
    ns.tprint('stop = Stop boot and init scripts');
    ns.tprint('status = Show status of boot and init scripts');
}
export async function main(ns) {
    if (ns.args.length === 0 || ns.args[0] === 'start') {
        ns.tprint('Initializing..');
        startBootScripts(ns, true);
        startInitScripts(ns, true);
        ns.tprint('Initialized.');
    }
    else if (ns.args[0] === 'stop') {
        stopBootScripts(ns, true);
        stopInitScripts(ns, true);
    }
    else if (ns.args[0] === 'status') {
        getBootScriptsStatus(ns, true);
        getInitScriptsStatus(ns, true);
    }
    else {
        help(ns);
    }
}
