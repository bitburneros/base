import { connect, findServer, getFlatNetwork, getNetwork, getOwnedServerCosts, getOwnedServers } from "lib/libnetwork";
import { getPlayer } from "lib/libplayer";
import { css, EventHandlerQueue, renderCustomModal, toolbarStyles } from "lib/libcommon";
const cmds = ['list', 'get', 'status', 'upgrade', 'buy', 'sell', 'ui'];
export function autocomplete(data, args) {
    if (args.length === 0 || !cmds.includes(args[0])) {
        return cmds;
    }
    else if (args[0] === 'list') {
        return [];
    }
    else if (args[0] === 'get') {
        return [];
    }
    else if (args[0] === 'status') {
        return [];
    }
    else if (args[0] === 'upgrade') {
        return [];
    }
    else if (args[0] === 'buy') {
        return [];
    }
    else if (args[0] === 'sell') {
        return [];
    }
    else {
        return [];
    }
}
export function help(ns) {
    ns.tprint('Servers Help');
    ns.tprint('servers <cmd> [args...]');
    ns.tprint('Commands');
    ns.tprint('list = List purchased servers');
    ns.tprint('get <server> = Get purchased server');
    ns.tprint('status = Show status of purchased servers');
    ns.tprint('upgrade [server] = Upgrade a purchased servers ram');
    ns.tprint('cost [ram=max] = Show cost of server with max or specified ram');
    ns.tprint('buy [ram=max] [amount=max] = Purchase max or specified amount of servers with specified ram');
    ns.tprint('sell [amount=max] = Sell all or specified amount of purchased servers');
}
export async function main(ns) {
    if (ns.args[0] === 'list') {
        const servers = getOwnedServers();
        if (servers) {
            servers.forEach((serverName) => {
                const server = findServer(serverName);
                if (server) {
                    ns.tprint(`${serverName}: ${JSON.stringify(server, null, 2)}`);
                }
            });
        }
    }
    else if (ns.args[0] === 'get' && ns.args[1]) {
        const server = findServer(ns.args[1]);
        if (server) {
            ns.tprint(`${ns.args[1]}: ${JSON.stringify(server, null, 2)}`);
        }
    }
    else if (ns.args[0] === 'status') {
    }
    else if (ns.args[0] === 'upgrade') {
    }
    else if (ns.args[0] === 'cost') {
        if (ns.args.length === 1) {
            const player = getPlayer();
            const ownedServerCosts = getOwnedServerCosts();
            if (player && ownedServerCosts) {
                const limit = ns.getPurchasedServerLimit();
                Object.keys(ownedServerCosts).forEach((ram) => {
                    const canBuy = player.money > ownedServerCosts[parseFloat(ram)];
                    const canBuyMax = player.money > (ownedServerCosts[parseFloat(ram)] * limit);
                    ns.tprint(`${ram} = $${ns.nFormat(ownedServerCosts[parseFloat(ram)], '0,0.00')} Buyable(${canBuy}) MaxBuyable(${canBuyMax})`);
                });
            }
        }
        else if (ns.args.length === 2) {
            const player = getPlayer();
            const ownedServerCosts = getOwnedServerCosts();
            if (player && ownedServerCosts) {
                const limit = ns.getPurchasedServerLimit();
                const ram = ns.args[1];
                if (ownedServerCosts[ram]) {
                    const canBuy = player.money > ownedServerCosts[ram];
                    const canBuyMax = player.money > (ownedServerCosts[ram] * limit);
                    ns.tprint(`${ram} = $${ns.nFormat(ownedServerCosts[ram], '0,0.00')} Buyable(${canBuy}) MaxBuyable(${canBuyMax})`);
                }
                else {
                    help(ns);
                }
            }
        }
    }
    else if (ns.args[0] === 'buy') {
        if (ns.args.length === 1) {
            const player = getPlayer();
            const servers = getOwnedServers();
            const ownedServerCosts = getOwnedServerCosts();
            if (player && servers && ownedServerCosts) {
                const limit = ns.getPurchasedServerLimit() - servers.length;
                let maxRam = 0;
                Object.keys(ownedServerCosts).forEach((ramAmount) => {
                    const ramPrice = ownedServerCosts[parseInt(ramAmount)];
                    if ((ramPrice * limit) < player.money) {
                        maxRam = parseInt(ramAmount);
                    }
                });
                const ram = maxRam;
                if (limit === 0) {
                    ns.tprint('Unable to purchase more servers!');
                }
                else {
                    const cost = ownedServerCosts[ram] * limit;
                    if (player.money > cost) {
                        for (let i = 0; i < limit; i++) {
                            const newServer = ns.purchaseServer('pserver', ram);
                            ns.tprint(`Purchased server ${newServer}`);
                        }
                    }
                    else {
                        const formattedCost = ns.nFormat(cost, '0,0.00');
                        const formattedMoney = ns.nFormat(player.money, '0,0.00');
                        ns.tprint(`Not enough money! You have $${formattedMoney}. Cost $${formattedCost}`);
                    }
                }
            }
        }
        else if (ns.args.length === 2) {
            const player = getPlayer();
            const servers = getOwnedServers();
            const ownedServerCosts = getOwnedServerCosts();
            if (player && servers && ownedServerCosts) {
                const limit = ns.getPurchasedServerLimit() - servers.length;
                let maxRam = 0;
                Object.keys(ownedServerCosts).forEach((ramAmount) => {
                    const ramPrice = ownedServerCosts[parseInt(ramAmount)];
                    if ((ramPrice * limit) < player.money) {
                        maxRam = parseInt(ramAmount);
                    }
                });
                const ram = (ns.args[1] === 'max' ? maxRam : ns.args[1]);
                const cost = ownedServerCosts[ram];
                if (player.money > (cost * limit)) {
                    for (let i = 0; i < limit; i++) {
                        const newServer = ns.purchaseServer('pserver', ram);
                        ns.tprint(`Purchased server ${newServer}`);
                    }
                }
                else {
                    ns.tprint('Not enough money');
                }
            }
        }
        else if (ns.args.length === 3) {
            const player = getPlayer();
            const servers = getOwnedServers();
            const ownedServerCosts = getOwnedServerCosts();
            if (player && servers && ownedServerCosts) {
                const limit = ns.getPurchasedServerLimit() - servers.length;
                let maxRam = 0;
                Object.keys(ownedServerCosts).forEach((ramAmount) => {
                    const ramPrice = ownedServerCosts[parseInt(ramAmount)];
                    if ((ramPrice * limit) < player.money) {
                        maxRam = parseInt(ramAmount);
                    }
                });
                const ram = (ns.args[1] === 'max' ? maxRam : ns.args[1]);
                const amount = (ns.args[2] === 'max' ? limit : ns.args[2]);
                if (amount < limit) {
                    ns.tprint(`Unable to purchase ${ns.args[2]} servers! Over limit by ${limit}!`);
                }
                else {
                    const cost = ownedServerCosts[ram] * amount;
                    if (player.money > cost) {
                        for (let i = 0; i < amount; i++) {
                            const newServer = ns.purchaseServer('pserver', ram);
                            ns.tprint(`Purchased server ${newServer}`);
                        }
                    }
                    else {
                        ns.tprint('Not enough money');
                    }
                }
            }
        }
        else {
            help(ns);
        }
    }
    else if (ns.args[0] === 'sell') {
        if (ns.args.length === 1) {
            const ownedServers = getOwnedServers();
            if (ownedServers) {
                ownedServers.forEach((host) => {
                    if (ns.deleteServer(host)) {
                        ns.tprint(`Deleted server ${host}`);
                    }
                });
            }
        }
        else if (ns.args.length === 2) {
            const ownedServers = getOwnedServers();
            if (ownedServers) {
                let i = 0;
                ownedServers.reverse().forEach((host) => {
                    if (i < ns.args[1]) {
                        if (ns.deleteServer(host)) {
                            ns.tprint(`Deleted server ${host}`);
                        }
                    }
                    i++;
                });
            }
        }
        else {
            help(ns);
        }
    }
    else if (ns.args[0] === 'ui') {
        let player = getPlayer();
        let network = getNetwork();
        const ownedServerCosts = getOwnedServerCosts();
        if (player && network && ownedServerCosts) {
            let servers = getFlatNetwork(network);
            const eventQueue = new EventHandlerQueue();
            const onRootAll = eventQueue.wrap(() => {
                ns.run('/bin/network.js', 1, 'hack');
            });
            const onBackdoorAll = eventQueue.wrap(() => {
                ns.run('/bin/network.js', 1, 'backdoor');
            });
            while (true) {
                player = getPlayer();
                network = getNetwork();
                servers = getFlatNetwork(network);
                const filteredServers = servers.filter((server) => (server.hostname !== 'home' &&
                    server.purchasedByPlayer));
                let upgradeCost = 0;
                servers.forEach((server) => {
                    const rams = Object.keys(ownedServerCosts);
                    const curRam = rams.findIndex((ram) => parseInt(ram) === server.maxRam);
                    const nextRam = parseInt(rams[curRam + 1]);
                    const canUpgrade = curRam !== rams.length;
                    if (canUpgrade) {
                        upgradeCost = upgradeCost + ownedServerCosts[nextRam];
                    }
                });
                const hasBulkUpgradeMoney = player && player.money > upgradeCost;
                const onBulkUpgradeClick = eventQueue.wrap(() => {
                    ns.toast('Upgrading...');
                    filteredServers.forEach((server) => {
                        const rams = Object.keys(ownedServerCosts);
                        const curRam = rams.findIndex((ram) => parseInt(ram) === server.maxRam);
                        const nextRam = parseInt(rams[curRam + 1]);
                        if (ns.deleteServer(server.hostname)) {
                            if (ns.purchaseServer(server.hostname, nextRam)) {
                                ns.toast(`Server(${server.hostname} upgraded to ${ns.nFormat(nextRam * 1000 * 1000 * 1000, '0b')})`);
                            }
                            else {
                                ns.toast(`Unable to purchase ${server.hostname}`);
                            }
                        }
                        else {
                            ns.toast(`Unable to delete server ${server.hostname}`);
                        }
                    });
                });
                const onBuyOneClick = eventQueue.wrap(() => {
                    ns.run('/bin/servers.js', 1, 'buy', 'max', 1);
                });
                const onBuyMaxClick = eventQueue.wrap(() => {
                    ns.run('/bin/servers.js', 1, 'buy');
                });
                ns.tail();
                renderCustomModal(ns, React.createElement("div", { id: 'custom-monitor', style: { fontSize: '0.75rem' } },
                    React.createElement("style", { children: css `
              #custom-monitor th,
              #custom-monitor td {
                  padding-right: 12px;
              }
              #custom-monitor th {
                  text-align: left;
              }
              #custom-monitor thead > * {
                  border-bottom: 1px solid green;
              }
              #custom-monitor tr:hover {
                  background: rgba(255, 255, 255, 0.1);
              }
          ` }),
                    React.createElement("div", { style: toolbarStyles },
                        React.createElement("button", { disabled: filteredServers.length === ns.getPurchasedServerLimit(), onClick: onBuyOneClick }, "Buy One"),
                        React.createElement("button", { disabled: filteredServers.length === ns.getPurchasedServerLimit(), onClick: onBuyMaxClick }, "Buy Max"),
                        React.createElement("button", { disabled: (!hasBulkUpgradeMoney || upgradeCost === 0), onClick: onBulkUpgradeClick }, "Upgrade All"),
                        React.createElement("button", { onClick: onRootAll }, "Root All"),
                        React.createElement("button", { onClick: onBackdoorAll }, "Backdoor All")),
                    React.createElement("table", { style: { borderSpacing: 0, whiteSpace: 'pre' } },
                        React.createElement("thead", null,
                            React.createElement("th", null, "Server"),
                            React.createElement("th", null, "Root"),
                            React.createElement("th", null, "Processes"),
                            React.createElement("th", null, "Used RAM"),
                            React.createElement("th", null, "Max RAM"),
                            React.createElement("th", null, "Tools")),
                        React.createElement("tbody", null, filteredServers.map((server) => {
                            const onKillAllClick = eventQueue.wrap(() => {
                                ns.ps(server.hostname).forEach(x => ns.kill(x.pid));
                            });
                            const onConnectClick = eventQueue.wrap(() => {
                                connect(ns, server);
                            });
                            const rams = Object.keys(ownedServerCosts);
                            const curRam = rams.findIndex((ram) => parseInt(ram) === server.maxRam);
                            const nextRam = parseInt(rams[curRam + 1]);
                            const canUpgrade = curRam !== rams.length;
                            const hasUpgradeMoney = player && player.money > ownedServerCosts[nextRam];
                            const onUpgradeClick = eventQueue.wrap(() => {
                                if (ns.deleteServer(server.hostname)) {
                                    if (ns.purchaseServer(server.hostname, nextRam)) {
                                        ns.toast(`Server(${server.hostname} upgraded to ${ns.nFormat(nextRam * 1000 * 1000 * 1000, '0b')})`);
                                    }
                                    else {
                                        ns.toast(`Unable to purchase ${server.hostname}`);
                                    }
                                }
                                else {
                                    ns.toast(`Unable to delete server ${server.hostname}`);
                                }
                            });
                            return (React.createElement("tr", { key: server.hostname },
                                React.createElement("th", null, server.hostname),
                                React.createElement("td", { style: { textAlign: "center" } }, server.hasAdminRights ? 'X' : ' '),
                                React.createElement("td", { style: { textAlign: "center" } }, ns.ps(server.hostname).length),
                                React.createElement("td", { style: { textAlign: "right" } }, ns.nFormat((Math.round(server.ramUsed * 10) / 10) * 1000 * 1000 * 1000, '0.0b')),
                                React.createElement("td", { style: { textAlign: "right" } }, ns.nFormat(server.maxRam * 1000 * 1000 * 1000, '0b')),
                                React.createElement("td", null,
                                    canUpgrade &&
                                        React.createElement("button", { disabled: !hasUpgradeMoney, onClick: onUpgradeClick, title: `Upgrade server to ${ns.nFormat(nextRam * 1000 * 1000 * 1000, '0b')} for ${ns.nFormat(ownedServerCosts[nextRam], '0.00a')}` }, "Upgrade"),
                                    React.createElement("button", { onClick: onConnectClick, title: 'Connect to this server' }, "Connect"),
                                    React.createElement("button", { onClick: onKillAllClick, title: 'Kill all scripts on this server' }, "Kill"))));
                        })))));
                await eventQueue.executeEvents();
                await ns.sleep(1000);
            }
        }
    }
    else {
        help(ns);
    }
}
