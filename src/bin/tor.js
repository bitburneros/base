const cmds = ['list', 'get', 'buy'];
export function autocomplete(data, args) {
    if (args.length === 0 || !cmds.includes(args[0])) {
        return cmds;
    }
    else if (args[0] === 'list') {
        return [];
    }
    else if (args[0] === 'get') {
        return [];
    }
    else if (args[0] === 'buy') {
        return [];
    }
    else {
        return [];
    }
}
export function help(ns) {
    ns.tprint('Tor Help');
    ns.tprint('tor <cmd> [args...]');
    ns.tprint('Commands');
    ns.tprint('list = List available software');
    ns.tprint('get = Get TOR Router');
    ns.tprint('buy [program] = Buy program from darkweb');
}
export async function main(ns) {
    const sourceFiles = JSON.parse(localStorage.sourceFiles);
    const player = JSON.parse(localStorage.player);
    if (player.bitNodeN === 4 || sourceFiles[4] !== undefined) {
        if (ns.args[0] === 'list') {
            if (player.tor) {
                const programs = ns.singularity.getDarkwebPrograms();
                const costs = programs.map((program) => { return ns.singularity.getDarkwebProgramCost(program); });
                for (let i in programs) {
                    if (!ns.fileExists(programs[i], 'home')) {
                        ns.tprint(`Program ${programs[i]} for $${ns.nFormat(costs[i], '0,0.00')}`);
                    }
                    else {
                        ns.tprint(`Program ${programs[i]} already owned`);
                    }
                }
            }
            else {
                ns.tprint('ERROR Tor router not installed! run "tor get"');
            }
        }
        else if (ns.args[0] === 'get') {
            if (player.tor) {
                ns.tprint('INFO Tor already purchased');
            }
            else {
                if (ns.singularity.purchaseTor()) {
                    ns.tprint('INFO Tor router purchased');
                }
                else {
                    ns.tprint('ERROR Unable to purchase tor router');
                }
            }
        }
        else if (ns.args[0] === 'buy') {
            if (ns.args.length === 1) {
                const programs = ns.singularity.getDarkwebPrograms();
                for (let i = 0; i < programs.length; i++) {
                    if (!ns.fileExists(programs[i], 'home')) {
                        if (ns.singularity.purchaseProgram(programs[i])) {
                            ns.tprint(`INFO Purchased ${programs[i]}`);
                        }
                        else {
                            ns.tprint(`INFO Unable to purchase ${programs[i]}`);
                        }
                    }
                }
            }
            else {
                for (let i = 0; i < ns.args.length; i++) {
                    if (i > 0) {
                        if (!ns.fileExists(ns.args[i], 'home')) {
                            if (ns.singularity.purchaseProgram(ns.args[i])) {
                                ns.tprint(`INFO Purchased ${ns.args[i]}`);
                            }
                            else {
                                ns.tprint(`INFO Unable to purchase ${ns.args[i]}`);
                            }
                        }
                        else {
                            ns.tprint(`ERROR Program(${ns.args[i]}) already purchased!`);
                        }
                    }
                }
            }
        }
        else {
            help(ns);
        }
    }
    else {
        ns.tprint('ERROR Need to be on BitNode 4 or have source file 4');
    }
}
