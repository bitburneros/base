export async function main(ns) {
    let cmds = [];
    ns.ls('home', '/bin').forEach(cmd => cmds.push(cmd));
    ns.ls('home', '/sbin').forEach(cmd => cmds.push(cmd));
    ns.ls('home', '/usr/bin').forEach(cmd => cmds.push(cmd));
    ns.ls('home', '/usr/sbin').forEach(cmd => cmds.push(cmd));
    cmds.forEach((cmd) => {
        const sCmd = cmd.split('/');
        const cmdName = sCmd[sCmd.length - 1].substring(0, sCmd[sCmd.length - 1].length - 3);
        ns.tprint(`alias ${cmdName}="run ${cmd}"`);
    });
}
