import {NS, AutocompleteData, Player} from '../../index';
import {getSourceFiles} from "lib/libbitnode";
import {getFactionInvites, getFactions, getOwnedAugmentations} from "lib/libfactions";
import {getPlayer} from "lib/libplayer";

const cmds = ['list', 'get', 'join', 'work', 'augments'];
export function autocomplete(data: AutocompleteData, args: string[]) {
  if (args.length === 0 || !cmds.includes(args[0])) {
    return cmds;
  }
  else if (args[0] === 'list') {
    return [];
  }
  else if (args[0] === 'get') {
    return [];
  }
  else if (args[0] === 'join') {
    return [];
  }
  else if (args[0] === 'work') {
    return [];
  }
  else {
    return [];
  }
}

export function help(ns: NS) {
  ns.tprint('Factions Help');
  ns.tprint('factions <cmd> [args...]');
  ns.tprint('Commands');
  ns.tprint('list = List factions joined or with invitations');
  ns.tprint('get <faction> = Get faction information');
  ns.tprint('join [faction] = Join all factions with invitations or specified');
  ns.tprint('work = Under Construction');
  ns.tprint('augments buy = Buy all available augmentations');
  ns.tprint('augments list = List all available augmentations');
  ns.tprint('augments <faction> <cmd> [args...] = Access augmentation commands');
  ns.tprint('augments <faction> list = Show list of augmentations');
  ns.tprint('augments <faction> buy [id] = Buy via id or all available augmentations');
}

export async function main(ns: NS) {
  const sourceFiles = getSourceFiles();
  if (sourceFiles && sourceFiles[4] !== undefined) {
    if (ns.args[0] === 'list') {
      let factions = getFactions();
      ns.tprint('My Factions');
      Object.keys(factions).forEach((faction, i) => ns.tprint(`(${i}) ${faction}`));
      const invites = getFactionInvites();
      if (invites && invites.length > 0) {
        ns.tprint('Faction Invites');
        invites.forEach((faction, i) => ns.tprint(`(${i}) ${faction}`));
      }
    }
    else if (ns.args[0] === 'get') {
      if (ns.args.length === 2) {
        let factions = getFactions();
        if (factions) {
          let factionName = Object.keys(factions)[ns.args[1] as number];
          let faction = factions[factionName];
          ns.tprint(factionName);
          ns.tprint(`Reputation: ${faction.reputation}`);
          ns.tprint(`Favor: ${faction.favor}`);
        }
      } else {
        if (ns.args.length) {

        }
      }
    }
    else if (ns.args[0] === 'join') {
      const invites = getFactionInvites();
      if (invites) {
        if (ns.args.length === 1) {
          invites.forEach(faction => ns.singularity.joinFaction(faction));
        } else {
          ns.singularity.joinFaction(invites[ns.args[1] as number]);
        }
      }
    }
    else if (ns.args[0] === 'work') {
    }
    else if (ns.args[0] === 'augments') {
      if (ns.args[1] === 'buy') {
        const factions = getFactions();
        if (factions) {
          Object.keys(factions).forEach((factionName) => {
            let faction = factions[factionName];
            faction.augmentations.forEach((augmentation) => {
              if ((augmentation.name === "NeuroFlux Governor" || !augmentation.owned) && augmentation.hasMoney && augmentation.hasReputation && augmentation.hasPrerequisites) {
                if (ns.singularity.purchaseAugmentation(factionName, augmentation.name)) {
                  ns.tprint(`Augment(${augmentation.name}) purchased!`);
                }
              }
            });
          });
        }
      }
      else if (ns.args[1] === 'list') {
        const factions = getFactions();
        if (factions) {
          Object.keys(factions).forEach((factionName) => {
            let faction = factions[factionName];
            faction.augmentations.forEach((augmentation) => {
              let formattedPrice = `$${ns.nFormat(augmentation.price, '0,0a')}`;
              let formattedRep = ns.nFormat(augmentation.reputation, '0,0');
              let prepend = `Buyable(${augmentation.hasReputation && augmentation.hasMoney})`;
              if (augmentation.owned) {
                prepend = `Owned`;
              }
              ns.tprint(`${prepend} Price(${formattedPrice}) Reputation(${formattedRep}) ${augmentation.name}`);
              if (augmentation.prerequisites.length > 0) {
                ns.tprint(`  Requires: ${augmentation.prerequisites.join(', ')}`);
              }
            });
          });
        }
      }
      else if (ns.args[2] === 'list') {
        const factions = getFactions();
        const factionName = Object.keys(factions)[ns.args[1] as number];
        if (factions) {
          let faction = factions[factionName];
          faction.augmentations.forEach((augmentation) => {
            let formattedPrice = `$${ns.nFormat(augmentation.price, '0,0a')}`;
            let formattedRep = ns.nFormat(augmentation.reputation, '0,0');
            let prepend = `Buyable(${augmentation.hasReputation && augmentation.hasMoney})`;
            if (augmentation.owned) {
              prepend = `Owned`;
            }
            ns.tprint(`${prepend} Price(${formattedPrice}) Reputation(${formattedRep}) ${augmentation.name}`);
            if (augmentation.prerequisites.length > 0) {
              ns.tprint(`  Requires: ${augmentation.prerequisites.join(', ')}`);
            }
          });
        }
      }
      else if (ns.args[2] === 'buy') {
        const player = getPlayer();
        const factions = getFactions();
        const factionName = Object.keys(factions)[ns.args[1] as number];
        if (factions && player) {
          const faction = factions[factionName];
          if (ns.args.length === 3) {
            faction.augmentations.forEach((augmentation) => {
              if ((augmentation.name === "NeuroFlux Governor" || !augmentation.owned) && augmentation.hasMoney && augmentation.hasReputation && augmentation.hasPrerequisites) {
                if (ns.singularity.purchaseAugmentation(factionName, augmentation.name)) {
                  ns.tprint(`Augment(${augmentation.name}) purchased!`);
                }
                else {
                  ns.tprint(`ERROR Unable to purchase augmentation ${augmentation.name}`)
                }
              }
            });
          } else {
            const augmentation = faction.augmentations[ns.args[3] as number];
            if (!augmentation.hasMoney) {
              ns.tprint(`Not enough money! Need $${ns.nFormat(augmentation.price - player.money, '0,0a')}`);
            } else if (!augmentation.hasReputation) {
              ns.tprint(`Not enough reputation! Need ${ns.nFormat(augmentation.reputation - faction.reputation, '0,0.00')}`);
            } else if (!augmentation.hasPrerequisites) {
              ns.tprint(`Missing required prerequisite augmentations!`);
            } else if (augmentation.hasMoney && augmentation.hasReputation && augmentation.hasPrerequisites) {
              if (ns.singularity.purchaseAugmentation(factionName, augmentation.name)) {
                ns.tprint(`Augment(${augmentation.name}) purchased!`);
              }
              else {
                ns.tprint(`ERROR Unable to purchase augmentation ${augmentation.name}`)
              }
            }
          }
        }
      }
    } else {
      help(ns);
    }
  }
}
