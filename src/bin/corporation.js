import { getCorporation } from "lib/libcorporation";
const cmds = ['create', 'status', 'update', 'list', 'get', 'upgrade'];
export function autocomplete(data, args) {
    if (args.length === 0 || !cmds.includes(args[0])) {
        return cmds;
    }
    else if (args[0] === 'create') {
        return [];
    }
    else if (args[0] === 'status') {
        return [];
    }
    else if (args[0] === 'update') {
        let packages = JSON.parse(localStorage.bpmPackages);
        return Object.keys(packages);
    }
    else if (args[0] === 'list') {
        return [];
    }
    else if (args[0] === 'get') {
        let packages = JSON.parse(localStorage.bpmPackages);
        return Object.keys(packages);
    }
    else {
        return [];
    }
}
export function help(ns) {
    ns.tprint('Corporation Help');
    ns.tprint('corporation <cmd> [args...]');
    ns.tprint('Commands');
    ns.tprint('create <name> [selfFund=true] = Create a corporation with name');
    ns.tprint('status = Show corporation status');
    ns.tprint('update <name> = Update BPM package');
    ns.tprint('upgrade = Upgrade BitBurnerOS');
    ns.tprint('list = Show list of installed packaged');
    ns.tprint('get <name> = Get package details by name');
}
export async function main(ns) {
    if (ns.args[0] === 'create' && ns.args.length >= 2) {
        const corporation = getCorporation();
        if (!corporation) {
            let args = [...ns.args];
            let selfFund = true;
            if (args[args.length - 1] === true || args[args.length - 1] === false) {
                selfFund = args.pop();
            }
            args.shift();
            let name = args.join(' ');
            if (ns.corporation.createCorporation(name, selfFund)) {
                ns.tprint(`Corporation(${name}) created!`);
            }
            else {
                ns.tprint('ERROR Unable to create corporation');
            }
        }
    }
    else if (ns.args[0] === 'status') {
        ns.tprint(JSON.stringify(getCorporation()));
    }
    else if (ns.args[0] === 'update' && ns.args[1]) {
    }
    else if (ns.args[0] === 'upgrade') {
    }
    else if (ns.args[0] === 'list') {
    }
    else if (ns.args[0] === 'get' && ns.args[1]) {
    }
    else {
        help(ns);
    }
}
