import {AutocompleteData, NS, Player} from "../../index";
import {
  findServer,
  networkTree,
  root,
  rootServers,
  backdoor,
  backdoorServers,
  getNetwork,
  getFlatNetwork, connect
} from "lib/libnetwork";
import {renderCustomModal, css, EventHandlerQueue, getColorScale, toolbarStyles} from 'lib/libcommon';
import type React_Type from 'react';
import {getPlayer} from "lib/libplayer";
declare var React: typeof React_Type;


const cmds = ['find', 'hack', 'backdoor', 'tree', 'ui'];
export function autocomplete(data: AutocompleteData, args: string[]) {
  if (args.length === 0 || !cmds.includes(args[0])) {
    return cmds;
  }
  else if (args[0] === 'find') {
    return data.servers;
  }
  else if (args[0] === 'hack' || args[0] === 'backdoor') {
    return data.servers;
  }
  else {
    return [];
  }
}

export function help(ns: NS) {
  ns.tprint('Network Help');
  ns.tprint('network <cmd> [args...]');
  ns.tprint('Commands');
  ns.tprint('find <server> = Find server');
  ns.tprint('hack [server] = Hack all or specified server');
  ns.tprint('backdoor [server] = Backdoor all or specified server');
  ns.tprint('tree [server] = Show network as a tree');
  ns.tprint('ui = Show Network UI');
}

export async function main(ns: NS) {
  if (ns.args[0] === 'find' && ns.args[1]) {
    const player = getPlayer();
    const network = getNetwork();
    const server = findServer(ns.args[1] as string);
    if (player && network && server !== null) {
      ns.tprint(`Hostname: ${server.hostname}`);
      ns.tprint(`Organization: ${server.organizationName}`);
      ns.tprint(`IP: ${server.ip}`);
      ns.tprint(`Rooted: ${server.hasAdminRights}`);
      ns.tprint(`Backdoor Installed: ${server.backdoorInstalled}`);
      ns.tprint(`Hardware: Cores(${server.cpuCores}) Memory(${ns.nFormat(server.ramUsed, '0b')})/${ns.nFormat(server.maxRam, '0b')})`);
      ns.tprint(`Ports: FTP(${server.ftpPortOpen}) SSH(${server.sshPortOpen}) SMTP(${server.smtpPortOpen}) HTTP(${server.httpPortOpen}) SQL(${server.sqlPortOpen}) ${server.openPortCount}/${server.numOpenPortsRequired}`);
      ns.tprint(`Difficulty: Base(${server.baseDifficulty}) Min(${server.minDifficulty}) Max(${server.maxRam})`);
      ns.tprint(`Money: ${ns.nFormat(server.moneyAvailable, '0,0a')}/${ns.nFormat(server.moneyMax, '0,0a')} (${server.moneyMax === 0 ? '' : Math.round(server.moneyAvailable / server.moneyMax * 1000) / 10}%)`);
      ns.tprint(`Hacking: Difficulty(${server.hackDifficulty}) Required(hacking ${server.requiredHackingSkill})`);
      ns.tprint(`Connect Command`);
      ns.tprint(`${server.path.map(name => `connect ${name};`).join(' ')} connect ${server.hostname}`);
      ns.tprint(`Analysis`);
      const moneyThresh = server.moneyMax * 0.75;
      const securityThresh = ns.getServerMinSecurityLevel(server.hostname);
      const securityLevel = ns.getServerSecurityLevel(server.hostname);
      const moneyAvailable = server.moneyMax;
      let action = 'hack';
      if (securityLevel > securityThresh) {
        action = 'weaken';
      }
      else if (moneyAvailable < moneyThresh) {
        action = 'grow';
      }
      ns.tprint(`Recommended Action: ${action}`);
      ns.tprint(`Weaken: ${ns.weakenAnalyze(1, 1)}`);
      ns.tprint(`Hack: ${ns.hackAnalyze(server.hostname)}`);
      ns.tprint(`Hack Threads: ${ns.hackAnalyzeThreads(server.hostname, moneyThresh)}`);
      ns.tprint(`Hack Security: ${ns.hackAnalyzeSecurity(1)}`);
      ns.tprint(`Hack Chance: ${ns.hackAnalyzeChance(server.hostname)}`);
      ns.tprint(`Grow: ${ns.growthAnalyze(server.hostname, 1, 1)}`);
      ns.tprint(`Grow Security: ${ns.growthAnalyzeSecurity(1)}`);
      if (ns.fileExists('Formulas.exe', 'home')) {
        ns.tprint(`Formulas`);
        ns.tprint(`Grow Percent: ${ns.formulas.hacking.growPercent(server, 1, player, 1)}`);
        ns.tprint(`Grow Time: ${ns.formulas.hacking.growTime(server, player)}`);
        ns.tprint(`Hack Chance: ${ns.formulas.hacking.hackChance(server, player)}`);
        ns.tprint(`Hack Experience: ${ns.formulas.hacking.hackExp(server, player)}`);
        ns.tprint(`Hack Percent: ${ns.formulas.hacking.hackPercent(server, player)}`);
        ns.tprint(`Weaken Time: ${ns.formulas.hacking.weakenTime(server, player)}`);

        let growTime = ns.getGrowTime(server.hostname);
        let weakenTime = ns.getWeakenTime(server.hostname);
        let hackTime = ns.getHackTime(server.hostname);
        let totalTime = hackTime + growTime + weakenTime;
        ns.tprint(`HGW Timing: ${hackTime} + ${growTime} + ${weakenTime} = ${Math.round(totalTime / 1000)} seconds`);
        const threshold = 0.3; //just as an example: get 30% of maxMoney
        const moneyPerSec = (server.moneyMax * threshold) / Math.round(totalTime / 1000);
        const hackPerc = ns.formulas.hacking.hackPercent(server, player);
        const hackThreads = Math.floor(threshold / hackPerc);
        const growPerc = ns.formulas.hacking.growPercent(server, 1, player);
        const growThreads = Math.ceil((1 / threshold) / (growPerc - 1));
        const weak = 0.05; //constant unless cores > 1
        const weakThreads = Math.ceil((ns.growthAnalyzeSecurity(growThreads) + ns.hackAnalyzeSecurity(hackThreads)) / weak);
        const moneyPerSecondPerThread = Math.ceil(moneyPerSec / (hackThreads + growThreads + weakThreads));
        ns.tprint(`HGW Threads: ${hackThreads}/${growThreads}/${growThreads}`);
        ns.tprint(`Money Per Second: ${ns.nFormat(moneyPerSec, '0,0a')}`);
        ns.tprint(`Money Per Second Per Thread: ${ns.nFormat(moneyPerSecondPerThread, '0,0a')}`);

      }
    } else {
      ns.tprint(`ERROR Server not found`);
    }
  }
  else if (ns.args[0] === 'hack' && ns.args[1]) {
    const server = findServer(ns.args[1] as string);
    if (server !== null) {
      if (server.hasAdminRights) {
        ns.tprint(`Server(${server.hostname}) already hacked!`);
      } else {
        root(ns, server);
      }
    } else {
      ns.tprint(`ERROR Server not found!`);
    }
  }
  else if (ns.args[0] === 'hack') {
    await rootServers(ns, getNetwork());
  }
  else if (ns.args[0] === 'backdoor' && ns.args[1]) {
    const server = findServer(ns.args[1] as string);
    if (server !== null) {
      if (server.backdoorInstalled) {
        ns.tprint(`Server(${server.hostname}) already has backdoor installed!`);
      } else {
        await backdoor(ns, server);
      }
    } else {
      ns.tprint(`ERROR Server not found!`);
    }
  }
  else if (ns.args[0] === 'backdoor') {
    await backdoorServers(ns, getNetwork());
  }
  else if (ns.args[0] === 'tree' && ns.args[1]) {
    const server = findServer(ns.args[1] as string);
    if (server !== null) {
      networkTree(ns, server);
    } else {
      ns.tprint(`ERROR Server not found!`);
    }
  }
  else if (ns.args[0] === 'tree') {
    networkTree(ns, getNetwork());
  }
  else if (ns.args[0] === 'ui') {
    if (getPlayer() && getNetwork()) {
      let showNonRooted = true;
      let showNonHackable = true;
      const eventQueue = new EventHandlerQueue();
      const onRootAll = eventQueue.wrap(() => {
        ns.run('/bin/network.js', 1, 'hack');
      });
      const onBackdoorAll = eventQueue.wrap(() => {
        ns.run('/bin/network.js', 1, 'backdoor');
      });
      while (true) {
        let player: Player = getPlayer() as Player;
        let network = getNetwork();
        if (player && network) {
          let servers = getFlatNetwork(network);
          let canOpenPortCount = 0;
          if (ns.fileExists('BruteSSH.exe', 'home')) canOpenPortCount++;
          if (ns.fileExists('FTPCrack.exe', 'home')) canOpenPortCount++;
          if (ns.fileExists('relaySMTP.exe', 'home')) canOpenPortCount++;
          if (ns.fileExists('HTTPWorm.exe', 'home')) canOpenPortCount++;
          if (ns.fileExists('SQLInject.exe', 'home')) canOpenPortCount++;
          network = getNetwork();
          servers = getFlatNetwork(network);
          const filteredServers = servers.filter((server) => (
              (showNonRooted || server.hasAdminRights) &&
              (showNonHackable || server.requiredHackingSkill <= player.hacking)
          ));
          let summary = {
            usedRam: 0,
            maxRam: 0,
            money: 0,
            maxMoney: 0
          };
          ns.tail();
          renderCustomModal(ns,
            <div id='custom-monitor' style={{ fontSize: '0.75rem' }}>
              <style children={css`
                #custom-monitor th,
                #custom-monitor td {
                    padding-right: 12px;
                }
                #custom-monitor th {
                    text-align: left;
                }
                #custom-monitor thead > * {
                    border-bottom: 1px solid green;
                }
                #custom-monitor tr:hover {
                    background: rgba(255, 255, 255, 0.1);
                }
            `} />
              <div style={toolbarStyles}>
                <button onClick={() => showNonRooted = !showNonRooted}>
                  {showNonRooted ? 'Show' : 'Hide'} non-rooted
                </button>
                <button onClick={() => showNonHackable = !showNonHackable}>
                  {showNonHackable ? 'Show' : 'Hide'} non-hackable
                </button>
                <button onClick={onRootAll}>
                  Root All
                </button>
                <button onClick={onBackdoorAll}>
                  Backdoor All
                </button>
              </div>
              <table style={{ borderSpacing: 0, whiteSpace: 'pre' }}>
                <thead>
                  <th>Server</th>
                  <th>Can Root</th>
                  <th>Can Hack</th>
                  <th>Root</th>
                  <th>Backdoor</th>
                  <th>Used RAM</th>
                  <th>Max RAM</th>
                  <th>Money</th>
                  <th>Money %</th>
                  <th>Max $</th>
                  <th>hDiff</th>
                  <th>mDiff</th>
                  <th>GWH</th>
                  <th>Tools</th>
                </thead>
                <tbody>
                {filteredServers.map((server) => {
                  const onKillAllClick = eventQueue.wrap(() => {
                    ns.ps(server.hostname).forEach(x => ns.kill(x.pid));
                  });
                  const onConnectClick = eventQueue.wrap(() => {
                    connect(ns, server);
                  });
                  const onInfoClick = eventQueue.wrap(() => {
                    ns.run('/bin/network.js', 1, 'find', server.hostname);
                  });
                  let GWH = 'H';

                  if (server.moneyAvailable < (server.moneyMax * 0.75)) {
                    GWH = 'G';
                  } else if (server.hackDifficulty > (server.minDifficulty + 10)) {
                    GWH = 'W';
                  }
                  summary.usedRam += server.ramUsed;
                  summary.maxRam += server.maxRam;
                  summary.money += server.moneyAvailable;
                  summary.maxMoney += server.moneyMax;
                  let canHack = server.requiredHackingSkill <= player.hacking;
                  let skillRequired = server.requiredHackingSkill - player.hacking;
                  let canRoot = server.openPortCount >= server.numOpenPortsRequired || canOpenPortCount >= server.numOpenPortsRequired;
                  let portsRequired = server.numOpenPortsRequired - canOpenPortCount;
                  return (
                    <tr key={server.hostname}>
                      <th>{''.padEnd((server.path.length + (server.hostname === 'home'?0:1)) * 2, ' ')}{server.hostname}</th>
                      <td style={{textAlign: "center"}}>{canRoot ? 'X' : portsRequired}</td>
                      <td style={{textAlign: "center"}}>{canHack ? 'X' : skillRequired}</td>
                      <td style={{textAlign: "center"}}>{server.hasAdminRights ? 'X' : ' '}</td>
                      <td style={{textAlign: "center"}}>{server.backdoorInstalled ? 'X' : ' '}</td>
                      <td style={{textAlign: "right"}}>{ns.nFormat((Math.round(server.ramUsed * 10) / 10) * 1000 * 1000 * 1000, '0.0b')}</td>
                      <td style={{textAlign: "right"}}>{ns.nFormat(server.maxRam * 1000 * 1000 * 1000, '0b')}</td>
                      <td style={{ color: getColorScale(server.moneyAvailable / server.moneyMax), textAlign: "right" }}>
                        {ns.nFormat(server.moneyAvailable, '$0.00a')}
                      </td>
                      <td style={{ color: getColorScale(server.moneyAvailable / server.moneyMax), textAlign: "right" }}>
                        {server.moneyMax === 0 ? '' : Math.round(server.moneyAvailable / server.moneyMax * 1000) / 10 + '%'}
                      </td>
                      <td style={{textAlign: "right"}}>
                        {ns.nFormat(server.moneyMax, '$0.00a')}
                      </td>
                      <td style={{ color: getColorScale(1 - (server.hackDifficulty - server.minDifficulty) / 10), textAlign: "right" }}>
                        {Math.round(server.hackDifficulty * 100) / 100}
                      </td>
                      <td style={{textAlign: "right"}}>
                        {server.minDifficulty}
                      </td>
                      <td style={{textAlign: "right"}}>
                        {GWH}
                      </td>
                      <td>
                        <button onClick={onConnectClick} title='Connect to this server'>
                          Connect
                        </button>
                        <button onClick={onKillAllClick} title='Kill all scripts on this server'>
                          Kill
                        </button>
                        <button onClick={onInfoClick} title='Show server information'>
                          Info
                        </button>
                      </td>
                    </tr>
                  );
                })}
                <tr key="summary">
                  <th>Summary</th>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                  <td style={{textAlign: "right"}}>{ns.nFormat(summary.usedRam * 1000 * 1000 * 1000, '0.0b')}</td>
                  <td style={{textAlign: "right"}}>{ns.nFormat(summary.maxRam * 1000 * 1000 * 1000, '0.0b')}</td>
                  <td style={{textAlign: "right"}}>{ns.nFormat(summary.money, '$0.00a')}</td>
                  <td/>
                  <td style={{textAlign: "right"}}>{ns.nFormat(summary.maxMoney, '$0.00a')}</td>
                  <td/>
                  <td/>
                  <td/>
                  <td/>
                </tr>
                </tbody>
              </table>
            </div>
          );
          await eventQueue.executeEvents();
          await ns.sleep(1_000);
        }
      }
    }
  }
  else {
    help(ns);
  }
}
