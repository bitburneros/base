import { getNetwork, getFlatNetwork } from "lib/libnetwork";
import { solve } from "lib/libcontracts";
const cmds = ['list', 'solve', 'ui'];
export function autocomplete(data, args) {
    if (args.length === 0 || !cmds.includes(args[0])) {
        return cmds;
    }
    else if (args[0] === 'list' || args[0] === 'solve') {
        return data.servers;
    }
    else {
        return [];
    }
}
export function help(ns) {
    ns.tprint('Contracts Help');
    ns.tprint('contracts <cmd> [args...]');
    ns.tprint('Commands');
    ns.tprint('list [server] = List all Contracts');
    ns.tprint('solve [server] [contract] = Solve all or specific contract on host');
    ns.tprint('ui = Show Contracts UI');
}
export async function main(ns) {
    if (ns.args[0] === 'list') {
        let contracts = [];
        const network = getNetwork();
        let servers = getFlatNetwork(network);
        servers.forEach((server) => {
            if (ns.args[1]) {
                if (server.hostname !== ns.args[1]) {
                    return true;
                }
            }
            const filenames = ns.ls(server.hostname);
            filenames.forEach((filename) => {
                if (filename.endsWith('.cct')) {
                    contracts.push({
                        server: server,
                        filename: filename,
                        type: ns.codingcontract.getContractType(filename, server.hostname),
                        data: ns.codingcontract.getData(filename, server.hostname),
                        description: ns.codingcontract.getDescription(filename, server.hostname),
                        numTriesRemaining: ns.codingcontract.getNumTriesRemaining(filename, server.hostname)
                    });
                }
            });
        });
        let lastHost = '';
        let padding = '';
        contracts.forEach((contract) => {
            if (lastHost === contract.server.hostname) {
                padding = ''.padStart(lastHost.length + 2, ' ');
            }
            else {
                padding = `${contract.server.hostname}: `;
            }
            ns.tprint(`INFO: ${padding}${contract.filename} (Type: ${contract.type}) with ${contract.numTriesRemaining} tries remaining`);
            lastHost = contract.server.hostname;
        });
    }
    else if (ns.args[0] === 'solve') {
        let contracts = [];
        const network = getNetwork();
        let servers = getFlatNetwork(network);
        servers.forEach((server) => {
            if (ns.args[1]) {
                if (server.hostname !== ns.args[1]) {
                    return true;
                }
            }
            const filenames = ns.ls(server.hostname);
            filenames.forEach((filename) => {
                if (filename.endsWith('.cct')) {
                    if (ns.args[2] && ns.args[2] !== filename) {
                        return true;
                    }
                    contracts.push({
                        server: server,
                        filename: filename,
                        type: ns.codingcontract.getContractType(filename, server.hostname),
                        data: ns.codingcontract.getData(filename, server.hostname),
                        description: ns.codingcontract.getDescription(filename, server.hostname),
                        numTriesRemaining: ns.codingcontract.getNumTriesRemaining(filename, server.hostname)
                    });
                }
            });
        });
        let lastHost = '';
        let padding = '';
        for (const contract of contracts) {
            if (lastHost === contract.server.hostname) {
                padding = ''.padStart(lastHost.length + 2, ' ');
            }
            else {
                padding = `${contract.server.hostname}: `;
            }
            let solved = await solve(contract.type, contract.data, contract.server.hostname, contract.filename, ns);
            if (solved) {
                ns.tprint(`INFO: ${padding}${contract.filename} (Type: ${contract.type}) Solved! ${solved}`);
            }
            else {
                ns.tprint(`ERROR: ${padding}${contract.filename} (Type: ${contract.type}) Failed!`);
            }
            lastHost = contract.server.hostname;
        }
    }
    else {
        help(ns);
    }
}
