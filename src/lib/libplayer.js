export function getPlayer() {
    if (localStorage.player) {
        return JSON.parse(localStorage.player);
    }
    else {
        return false;
    }
}
export function updatePlayer(ns) {
    localStorage.player = JSON.stringify(ns.getPlayer());
}
