import { getPlayer } from "lib/libplayer";
import { getSourceFiles } from "lib/libbitnode";
export function getFactions() {
    if (localStorage.factions) {
        return JSON.parse(localStorage.factions);
    }
    else {
        return false;
    }
}
export function getFactionInvites() {
    if (localStorage.factionInvites) {
        return JSON.parse(localStorage.factionInvites);
    }
    else {
        return false;
    }
}
export function getOwnedAugmentations() {
    if (localStorage.ownedAugmentations) {
        return JSON.parse(localStorage.ownedAugmentations);
    }
    else {
        return false;
    }
}
export function updateFactions(ns) {
    const player = getPlayer();
    const sourceFiles = getSourceFiles();
    if (sourceFiles && sourceFiles[4] >= 0) {
        let factions = {};
        if (player) {
            let ownedAugmentations = ns.singularity.getOwnedAugmentations(true);
            player.factions.forEach((faction) => {
                let factionRep = ns.singularity.getFactionRep(faction);
                factions[faction] = {
                    reputation: ns.singularity.getFactionRep(faction),
                    favor: ns.singularity.getFactionFavor(faction),
                    favorGain: ns.singularity.getFactionFavorGain(faction),
                    augmentations: []
                };
                let augmentations = ns.singularity.getAugmentationsFromFaction(faction);
                factions[faction].augmentations = augmentations.map((augName) => {
                    let preReq = ns.singularity.getAugmentationPrereq(augName);
                    let factionInfo = {
                        name: augName,
                        price: ns.singularity.getAugmentationPrice(augName),
                        reputation: ns.singularity.getAugmentationRepReq(augName),
                        hasMoney: false,
                        hasReputation: false,
                        owned: ownedAugmentations.includes(augName),
                        hasPrerequisites: !preReq.map(pr => {
                            return ownedAugmentations.includes(pr);
                        }).includes(false),
                        prerequisites: preReq
                    };
                    factionInfo.hasMoney = factionInfo.price < player.money;
                    factionInfo.hasReputation = factionInfo.reputation < factionRep;
                    return factionInfo;
                });
            });
            localStorage.factions = JSON.stringify(factions);
            localStorage.factionInvites = JSON.stringify(ns.singularity.checkFactionInvitations());
            let oAugs = {};
            ownedAugmentations.forEach((augName) => {
                oAugs[augName] = ns.singularity.getAugmentationStats(augName);
            });
            localStorage.ownedAugmentations = JSON.stringify(oAugs);
        }
    }
}
