import { getPlayer } from "lib/libplayer";
import { drawTable, range, sleep } from "lib/libcommon";
export function getOwnedServers() {
    if (localStorage.ownedServers) {
        return JSON.parse(localStorage.ownedServers);
    }
    else {
        return false;
    }
}
export function getOwnedServerCosts() {
    if (localStorage.ownedServerCosts) {
        return JSON.parse(localStorage.ownedServerCosts);
    }
    else {
        return false;
    }
}
export function getNetwork() {
    if (localStorage.network) {
        return JSON.parse(localStorage.network);
    }
    else {
        return false;
    }
}
export function getFlatNetwork(server, flatNetwork = []) {
    flatNetwork.push(server);
    if (server.servers.length > 0) {
        server.servers.forEach((server) => {
            getFlatNetwork(server, flatNetwork);
        });
    }
    return flatNetwork;
}
export async function mapNetwork(ns, host, parent, path = []) {
    let server = ns.getServer(host);
    server.servers = [];
    server.path = [...path];
    if (host !== 'home')
        path.push(host);
    let servers = ns.scan(host);
    for (let name of servers) {
        if (name !== host && name !== parent) {
            server.servers.push(await mapNetwork(ns, name, host, [...path]));
        }
    }
    return server;
}
export function findServer(host, network = JSON.parse(localStorage.network)) {
    for (let server of network.servers) {
        if (server.hostname === host) {
            return server;
        }
        const find = findServer(host, server);
        if (find !== null) {
            return find;
        }
    }
    return null;
}
export function networkTree(ns, server, spacing = 0) {
    let spacer = "";
    for (let i = 0; i < spacing; i++) {
        spacer += "  ";
    }
    ns.tprint(`${spacer} ${server.hostname}${(server.hasAdminRights ? ' (rooted)' : '')}${(server.backdoorInstalled ? ' (backdoor)' : '')}`);
    for (let s of server.servers) {
        networkTree(ns, s, spacing + 1);
    }
}
export function connect(ns, server) {
    let connected = true;
    let curPath = [];
    ns.singularity.connect('home');
    server.path.forEach((item) => {
        if (connected && ns.singularity.connect(item)) {
            curPath.push(item);
            // ns.tprint(`Connected To ${item}`);
        }
        else {
            connected = false;
            ns.tprint(`Failed To connect to ${item}`);
        }
    });
    if (ns.singularity.connect(server.hostname)) {
        ns.tprint(`Connected To ${server.hostname}`);
    }
    else {
        curPath.reverse().forEach((item) => {
            ns.singularity.connect(item);
        });
        ns.singularity.connect('home');
        ns.tprint(`Failed To connect to ${server.hostname}`);
    }
    return connected;
}
export function disconnect(ns, server) {
    let connected = true;
    let curPath = [];
    server.path.reverse().forEach((item) => {
        if (connected && ns.singularity.connect(item)) {
            curPath.push(item);
            ns.tprint(`Connected To ${item}`);
        }
        else {
            connected = false;
            ns.tprint(`Failed To connect to ${item}`);
        }
    });
    if (ns.singularity.connect('home')) {
        ns.tprint('Connected To home');
    }
    else {
        ns.tprint('Failed To connect to home');
    }
    return connected;
}
export function root(ns, server) {
    let hacked = false;
    let portsOpened = 0;
    let portsRequired = server.numOpenPortsRequired;
    if (portsRequired > portsOpened && ns.fileExists("BruteSSH.exe", "home")) {
        ns.brutessh(server.hostname);
        portsOpened++;
    }
    if (portsRequired > portsOpened && ns.fileExists("FTPCrack.exe", "home")) {
        ns.ftpcrack(server.hostname);
        portsOpened++;
    }
    if (portsRequired > portsOpened && ns.fileExists("relaySMTP.exe", "home")) {
        ns.relaysmtp(server.hostname);
        portsOpened++;
    }
    if (portsRequired > portsOpened && ns.fileExists("HTTPWorm.exe", "home")) {
        ns.httpworm(server.hostname);
        portsOpened++;
    }
    if (portsRequired > portsOpened && ns.fileExists("SQLInject.exe", "home")) {
        ns.sqlinject(server.hostname);
        portsOpened++;
    }
    if (portsOpened >= portsRequired) {
        if (portsOpened > 0) {
            ns.tprint(`Server(${server.hostname}): Opened ${portsOpened}/${portsRequired} ports`);
        }
        ns.tprint(`Server(${server.hostname}): Rooting`);
        ns.nuke(server.hostname);
        hacked = true;
        ns.tprint(`Server(${server.hostname}): Rooted`);
    }
    return hacked;
}
export function rootServers(ns, network) {
    for (let server of network.servers) {
        if (server.hasAdminRights === false) {
            root(ns, server);
        }
        rootServers(ns, server);
    }
}
export async function backdoor(ns, server) {
    const player = getPlayer();
    if (player && server.hasAdminRights && !server.backdoorInstalled && server.requiredHackingSkill <= player.hacking) {
        if (connect(ns, server)) {
            await ns.singularity.installBackdoor();
            disconnect(ns, server);
        }
    }
}
export async function backdoorServers(ns, network) {
    for (let server of network.servers) {
        await backdoor(ns, server);
        await backdoorServers(ns, server);
    }
}
/*
  Obtained from https://github.com/jjclark1982/bitburner-scripts/blob/main/lib/port-service.js and modified
 */
/**
 * Get a service from a Netscript port.
 * Returns undefined if no service is running right now.
 * @param {NS} ns
 * @param {number} portNum
 */
export function getService(ns, portNum) {
    const portHandle = ns.getPortHandle(portNum);
    if (!portHandle.empty()) {
        return portHandle.peek();
    }
    return undefined;
}
/**
 * Get a service from a Netscript port.
 * Wait up to 2.5 seconds for the service to start (such as after reloading from save).
 * @param {NS} ns
 * @param {number} portNum
 */
export async function waitForService(ns, portNum) {
    const port = ns.getPortHandle(portNum);
    let tries = 50;
    while (port.empty() && tries-- > 0) {
        await ns.asleep(50);
    }
    if (port.empty()) {
        return null;
    }
    return port.peek();
}
/**
 * @typedef {Object} PortService - Service that makes an object available to other proecesses through a Netscript port.
 */
export class PortService {
    constructor(ns, portNum = 1, obj) {
        this.ns = ns;
        this.portNum = portNum;
        this.running = false;
        this.portHandle = ns.getPortHandle(portNum);
        this.publishObject(obj);
    }
    publishObject(obj) {
        const { ns } = this;
        obj || (obj = this);
        obj._service = this;
        this.object = obj;
        this.objectClassName = obj.constructor.name;
        this.objectName || (this.objectName = this.objectClassName.substr(0, 1).toLowerCase() + this.objectClassName.substr(1));
        // Replace any existing service on the same port.
        if (!this.portHandle.empty()) {
            const otherObj = this.portHandle.read();
            if ((otherObj === null || otherObj === void 0 ? void 0 : otherObj._service) && otherObj._service !== this) {
                otherObj._service.running = false;
                // await ns.asleep(1000);
            }
        }
        // Publish this service on the port.
        this.portHandle.clear();
        this.portHandle.write(this.object);
        // Publish this service in the browser's developer console.
        eval('window')[this.objectName] = this.object;
        eval('window')[`port${this.portNum}`] = this.object;
        // Unpublish this service when the process ends for any reason.
        ns.atExit(this.tearDown.bind(this));
        ns.tprint(`INFO: Started ${this.objectClassName} Service on port ${this.portNum}`);
    }
    // Block until something sets `this.running` to false.
    async serve() {
        const { ns } = this;
        ns.disableLog("asleep");
        this.running = true;
        while (this.running) {
            if (typeof (this.object.update) === "function") {
                this.object.update();
            }
            if (typeof (this.object.report) === "function") {
                ns.clearLog();
                ns.print(this.object.report());
            }
            await ns.asleep(1000);
        }
        this.tearDown();
        ns.tprint(`Stopped ${this.objectClassName} Service on port ${this.portNum}`);
    }
    tearDown() {
        var _a;
        if (this.object !== this && typeof ((_a = this.object) === null || _a === void 0 ? void 0 : _a.tearDown) === "function") {
            this.object.tearDown();
        }
        this.running = false;
        if (this.portHandle.peek() === this.object) {
            this.portHandle.read();
        }
        if (eval('window')[this.objectName] === this.object) {
            delete eval('window')[this.objectName];
        }
        if (eval('window')[`port${this.portNum}`] === this) {
            delete eval('window')[`port${this.portNum}`];
        }
    }
}
/**
 * Batch
 * @typedef {Array} Batch - array of jobs with methods for calculating useful metrics
 *
 * Jobs are ordered by their endTime and there is a clear firstEndTime and lastEndTime,
 * but the earliestStartTime also depends on other timing factors.
 */
export class Batch extends Array {
    summary() {
        const tasks = this.map((job) => (job.task || '-').substr(0, 1).toUpperCase());
        return tasks.join('');
    }
    longSummary() {
        const tasks = this.map((job) => (job.task || '-').substr(0, 1).toLowerCase() + job.threads);
        return tasks.join(' ');
    }
    peakThreads() {
        return this.reduce((total, job) => (total + job.threads), 0);
    }
    avgThreads(tDelta) {
        const threadMSeconds = this.reduce((total, job) => (total + job.threads * job.duration), 0);
        return threadMSeconds / this.totalDuration(tDelta);
    }
    maxThreads() {
        return this.reduce((total, job) => (Math.max(total, job.threads)), 0);
    }
    peakRam() {
        return this.reduce((total, job) => (total + job.threads * (TASK_RAM[job.task] || TASK_RAM['*'])), 0);
    }
    avgRam(tDelta) {
        const gbMSeconds = this.reduce((total, job) => {
            const gb = TASK_RAM[job.task] || 2.0;
            return total + job.threads * gb * job.duration;
        }, 0);
        return gbMSeconds / this.totalDuration(tDelta);
    }
    /**
     * @returns {number} The total amount of money gained by the player if all hack operations are successful.
     */
    moneyTaken() {
        return this.reduce((total, job) => {
            var _a;
            return (total + (((_a = job.change) === null || _a === void 0 ? void 0 : _a.playerMoney) || 0));
        }, 0);
    }
    /**
     * @returns {number} The effective percent of money hacked by the largest hacking job.
     */
    actualMoneyPercent() {
        const minMoneyMult = this.reduce((total, job) => (Math.min(total, job.change.moneyMult)), 1);
        return 1 - minMoneyMult;
    }
    moneySummary() {
        const moneyPercent = this.actualMoneyPercent();
        return `${moneyPercent < 0.09999 ? ' ' : ''}${(moneyPercent * 100).toFixed(1)}%`;
    }
    activeDuration(tDelta = 100) {
        return this.length * tDelta;
    }
    maxDuration() {
        return this.reduce((longest, job) => (Math.max(longest, job.duration)), 0);
    }
    /**
     * @returns {number} Total milliseconds from the start of the earliest job until after the end of the last job
     */
    totalDuration(tDelta = 100) {
        if (this.length == 0) {
            return 0;
        }
        if (!this.firstEndTime()) {
            this.setFirstEndTime(1, tDelta);
        }
        let earliestStart = this.earliestStartTime();
        return this.lastEndTime() + tDelta - (earliestStart ? earliestStart : 0);
        // return this.maxDuration() + this.activeDuration(tDelta);
    }
    /**
     * @returns {number} endTime of the first job, or null
     */
    firstEndTime() {
        var _a;
        return (_a = this[0]) === null || _a === void 0 ? void 0 : _a.endTime;
    }
    /**
     * @returns {number} endTime of the last job, or null
     */
    lastEndTime() {
        var _a;
        return (_a = this[this.length - 1]) === null || _a === void 0 ? void 0 : _a.endTime;
    }
    /**
     * @returns {number} The earliest startTime of any job, or Infinity if none have a startTime, or null if no jobs
     */
    earliestStartTime() {
        if (this.length == 0) {
            return null;
        }
        return this.reduce((e, job) => (Math.min(e, job.startTime)), Infinity);
    }
    /**
     * Modify all `startTime` and `endTime` values so that the first `endTime` is `firstEndTime`.
     * @param {number} firstEndTime
     * @param {number} tDelta
     */
    setFirstEndTime(firstEndTime, tDelta = 100) {
        let endTime = firstEndTime;
        for (const job of this) {
            job.endTime = endTime;
            endTime += tDelta;
            job.startTime = job.endTime - job.duration;
        }
    }
    /**
     * Modify all `startTime` and `endTime` values so that the earliest `startTime` is `startTime`.
     * @param {number} startTime
     * @param {number} tDelta
     */
    setStartTime(startTime, tDelta = 100) {
        if (this.length > 0) {
            if (!this[0].startTime) {
                this.setFirstEndTime(startTime + this[0].duration, tDelta);
            }
            const earliestStart = this.earliestStartTime();
            if ((earliestStart ? earliestStart : 0) < startTime) {
                this.adjustSchedule(startTime - (earliestStart ? earliestStart : 0));
            }
        }
    }
    adjustSchedule(offset) {
        if (offset) {
            for (const job of this) {
                job.startTime += offset;
                job.endTime += offset;
            }
        }
    }
    /**
     * Adjust all `startTime` and `endTime` values to be in the future.
     * @param {number} now - current timestamp
     * @param {number} tDelta - milliseconds between endTimes
     */
    ensureStartInFuture(now, tDelta) {
        now || (now = Date.now());
        let earliestStart = this.earliestStartTime();
        if (!((earliestStart ? earliestStart : 0) > now)) {
            this.setStartTime(now + 1, tDelta);
        }
    }
    maxBatchesAtOnce(maxTotalRam, tDelta = 100, reserveRam = false) {
        const totalDuration = this.totalDuration(tDelta);
        const activeDuration = this.activeDuration(tDelta);
        const maxBatchesPerCycle = Math.floor(totalDuration / activeDuration);
        const ramUsed = reserveRam ? this.peakRam() : this.avgRam(tDelta);
        const maxBatchesInRam = Math.floor(maxTotalRam / ramUsed);
        return Math.min(maxBatchesPerCycle, maxBatchesInRam);
    }
    minTimeBetweenBatches(maxTotalRam, tDelta = 100) {
        const totalDuration = this.totalDuration(tDelta);
        const numBatchesAtOnce = this.maxBatchesAtOnce(maxTotalRam, tDelta);
        return (totalDuration / numBatchesAtOnce);
    }
}
const TASK_RAM = {
    null: 1.6,
    'hack': 1.7,
    'grow': 1.75,
    'weaken': 1.75,
    '*': 2.0
};
/*
  Original from https://github.com/jjclark1982/bitburner-scripts/blob/main/net/server-list.js and modified
 */
export class ServerList {
    constructor(ns, params = {}) {
        this.ns = ns;
        this.ServerClass = ServerModel;
        ns.disableLog("scan");
        Object.assign(this, params);
        delete this._cachedServers;
    }
    [Symbol.iterator]() {
        return Object.values(this.getAllServers())[Symbol.iterator]();
    }
    loadServer(hostname) {
        return new this.ServerClass(this.ns, hostname);
    }
    getAllServers() {
        if (!this._cachedServers) {
            const allServers = {};
            for (const hostname of this.getAllHostnames()) {
                allServers[hostname] = this.loadServer(hostname);
            }
            this._cachedServers = allServers;
            setTimeout(() => {
                delete this._cachedServers;
            }, 1);
        }
        return this._cachedServers;
    }
    getAllHostnames() {
        return getAllHostnames(this.ns);
    }
    getScriptableServers() {
        return [...this].filter((server) => (server.canRunScripts()));
    }
    getHackableServers(player) {
        return [...this].filter((server) => (server.canBeHacked(player)));
    }
    getHacknetServers() {
        return [...this].filter((server) => (server.hashCapacity));
    }
    getPlayerOwnedServers() {
        // includes purchased servers but not 'home' or hacknet servers
        return [...this].filter((server) => (server.purchasedByPlayer &&
            !(server.hostname == 'home') &&
            !(server.hashCapacity)));
    }
    getSmallestServersWithThreads(scriptRam, threads, exclude = {}) {
        const smallestServers = [...this].filter((server) => (!(server.hostname in exclude) &&
            server.availableThreads(scriptRam) >= threads)).sort((a, b) => (a.availableThreads(scriptRam) - b.availableThreads(scriptRam)));
        return smallestServers;
    }
    getBiggestServers(scriptRam, exclude = {}) {
        const biggestServers = [...this].filter((server) => (!(server.hostname in exclude) &&
            server.availableThreads(scriptRam) >= 1)).sort((a, b) => (b.maxRam - a.maxRam));
        return biggestServers;
    }
    totalRamUsed() {
        return this.getScriptableServers().reduce((total, server) => (total + server.ramUsed), 0);
    }
    totalRam() {
        return this.getScriptableServers().reduce((total, server) => (total + server.maxRam), 0);
    }
    totalThreadsAvailable(scriptRam = 1.75) {
        return this.getScriptableServers().reduce((total, server) => (total + server.availableThreads(scriptRam)), 0);
    }
    maxThreadsAvailable(scriptRam = 1.75) {
        return this.getScriptableServers().reduce((total, server) => (Math.max(total, server.availableThreads(scriptRam))), 0);
    }
}
export class ServerModel {
    constructor(ns, server) {
        this.ns = ns;
        this.__proto__.ns = ns;
        if (typeof (server) === 'string') {
            this.hostname = server;
            server = undefined;
        }
        this.reload(server);
    }
    reload(data) {
        data || (data = this.ns.getServer(this.hostname));
        Object.assign(this, data);
        return this;
    }
    copy() {
        return new this.constructor(this.ns, this);
    }
    canRunScripts() {
        return (this.hasAdminRights &&
            this.maxRam > 0);
    }
    canBeHacked(player) {
        // player ||= this.ns.getPlayer();
        return (this.hasAdminRights &&
            this.moneyMax > 0 &&
            this.requiredHackingSkill <= player.hacking);
    }
    availableRam(reservedRam = 0) {
        // by default, reserve up to 1TB of RAM on special servers
        if ((this.hostname === "home") || this.hashCapacity) {
            reservedRam || (reservedRam = Math.min(1024, this.maxRam * 3 / 4));
        }
        return Math.max(0, this.maxRam - this.ramUsed - reservedRam);
    }
    availableThreads(scriptRam = 1.75) {
        if (!this.canRunScripts()) {
            return 0;
        }
        return Math.floor(this.availableRam() / scriptRam) || 0;
    }
    /**
     * getStockInfo - Load stock info from a stock service
     * @param {number} portNum - netscript port with a stock service
     * @returns {object} stockInfo {symbol, netShares, netValue, [forecast], [volatility]}
     */
    getStockInfo(portNum = 5) {
        const { ns } = this;
        if ("stockInfo" in this) {
            return this.stockInfo;
        }
        this.stockInfo = null;
        if (this.organizationName) {
            const port = ns.getPortHandle(portNum);
            if (!port.empty()) {
                const stockService = port.peek();
                if (typeof (stockService.getStockInfo) == 'function') {
                    this.stockInfo = stockService.getStockInfo(this.organizationName);
                }
            }
        }
        // cache this info for 1 ms, so many calls can be made in the same tick
        setTimeout(() => {
            delete this.stockInfo;
        }, 1);
        return this.stockInfo;
    }
}
export function getAllHostnames(ns) {
    var _a;
    (_a = getAllHostnames).cache || (_a.cache = new Set());
    const scanned = getAllHostnames.cache;
    const toScan = ['home'];
    while (toScan.length > 0) {
        const hostname = toScan.shift();
        if (hostname) {
            scanned.add(hostname);
            for (const nextHost of ns.scan(hostname)) {
                if (!scanned.has(nextHost)) {
                    toScan.push(nextHost);
                }
            }
        }
    }
    return scanned;
}
/**
 * A HackableServer tracks the state of a server through multiple hacking operations.
 * It has all the fields of a Netscript Server object, plus methods to mutate state.
 */
export class HackableServer extends ServerModel {
    reload(data) {
        data || (data = this.ns.getServer(this.hostname));
        Object.assign(this, data);
        this.prepDifficulty = this.hackDifficulty;
        return this;
    }
    isPrepared(secMargin = 0.75, moneyMargin = 0.125) {
        return (this.hackDifficulty < this.minDifficulty + secMargin &&
            this.moneyAvailable > this.moneyMax * (1 - moneyMargin));
    }
    preppedCopy(secMargin = 0) {
        const server = this.copy();
        server.moneyAvailable = server.moneyMax;
        server.hackDifficulty = server.minDifficulty;
        server.prepDifficulty = server.minDifficulty + secMargin;
        return server;
    }
    /**
     * Plan a "hack" job and mutate this server state to reflect its results.
     * @param {number} [moneyPercent=0.05] - Amount of money to hack, from 0.0 to 1.0
     * @param {number} [maxThreads]
     * @param {boolean} [stock=false] - whether to manipulate stock prices
     * @returns {Job}
     */
    planHack(moneyPercent = 0.05, maxThreads = Infinity, stock = false) {
        const { ns } = this;
        const server = this;
        const player = ns.getPlayer();
        // Calculate duration based on last known security level
        const duration = ns.formulas.hacking.hackTime(Object.assign(Object.assign({}, server), { hackDifficulty: this.prepDifficulty }), player);
        // Calculate threads
        moneyPercent = Math.max(0, Math.min(1.0, moneyPercent));
        const hackPercentPerThread = ns.formulas.hacking.hackPercent(server, player) || 0.00001;
        let threads = Math.ceil(moneyPercent / hackPercentPerThread);
        if (threads > maxThreads) {
            // Split threads evenly among jobs
            const numJobs = Math.ceil(threads / maxThreads);
            threads = Math.ceil(threads / numJobs);
        }
        // Calculate result
        const effectivePercent = Math.min(1, threads * hackPercentPerThread);
        const moneyMult = 1 - effectivePercent;
        const moneyChange = this.moneyAvailable * -effectivePercent;
        this.moneyAvailable = this.moneyAvailable * moneyMult;
        const securityChange = ns.hackAnalyzeSecurity(threads);
        this.hackDifficulty += securityChange;
        // Construct job
        const job = {
            task: 'hack',
            args: [server.hostname, { threads, stock }],
            threads: threads,
            duration: duration,
            change: { security: securityChange, moneyMult, money: moneyChange, playerMoney: -moneyChange },
            result: this.copy(),
        };
        return job;
    }
    /**
     * Plan a "grow" job and mutate this server state to reflect its results.
     * @param {number} [maxThreads]
     * @param {number} [cores=1]
     * @param {boolean} [stock=false] - whether to manipulate stock prices
     * @returns {Job}
     */
    planGrow(maxThreads = Infinity, cores = 1, stock = false) {
        const { ns } = this;
        const server = this;
        const player = ns.getPlayer();
        // Calculate duration based on last known security level
        const duration = ns.formulas.hacking.growTime(Object.assign(Object.assign({}, server), { hackDifficulty: this.prepDifficulty }), player);
        // Calculate threads using binary search
        let loThreads = 1;
        let hiThreads = maxThreads;
        if (!hiThreads || hiThreads < 1 || hiThreads == Infinity) {
            // Establish an upper bound based on the single-thread formula which will be too high.
            const growMult = server.moneyMax / Math.min(server.moneyMax, (server.moneyAvailable + 1));
            const growMultPerThread = ns.formulas.hacking.growPercent(server, 1, player, cores);
            hiThreads = Math.ceil((growMult - 1) / (growMultPerThread - 1)) + 1;
        }
        while (hiThreads - loThreads > 1) {
            const midThreads = Math.ceil((loThreads + hiThreads) / 2);
            const serverGrowth = ns.formulas.hacking.growPercent(server, midThreads, player, cores);
            const newMoney = (server.moneyAvailable + midThreads) * serverGrowth;
            if (newMoney >= server.moneyMax) {
                hiThreads = midThreads;
            }
            else {
                loThreads = midThreads;
            }
        }
        const threads = hiThreads;
        // Calculate result
        const prevMoney = this.moneyAvailable;
        const moneyMult = ns.formulas.hacking.growPercent(server, threads, player, cores);
        this.moneyAvailable = Math.min(this.moneyMax, (this.moneyAvailable + threads) * moneyMult);
        const moneyChange = this.moneyAvailable - prevMoney;
        const securityChange = ns.growthAnalyzeSecurity(threads);
        this.hackDifficulty += securityChange;
        // Construct job
        const job = {
            task: 'grow',
            args: [server.hostname, { threads, stock }],
            threads: threads,
            duration: duration,
            change: { security: securityChange, moneyMult, money: moneyChange, playerMoney: 0 },
            result: this.copy(),
        };
        return job;
    }
    /**
     * Plan a 'weaken' job and mutate this server state to reflect its results.
     * @param {number} [maxThreads]
     * @param {number} [cores=1]
     * @returns {Job}
     */
    planWeaken(maxThreads = Infinity, cores = 1) {
        const { ns } = this;
        const server = this;
        const player = ns.getPlayer();
        // Calculate duration based on last known security level
        const duration = ns.formulas.hacking.weakenTime(Object.assign(Object.assign({}, server), { hackDifficulty: this.prepDifficulty }), player);
        // Calculate threads
        const securityPerThread = -ns.weakenAnalyze(1, cores);
        const neededSecurity = server.minDifficulty - server.hackDifficulty - 1;
        const threads = Math.min(
        // Split threads with larger jobs first.
        maxThreads, Math.ceil(neededSecurity / securityPerThread));
        // Calculate result
        this.prevDifficulty = this.hackDifficulty;
        this.hackDifficulty = Math.max(this.minDifficulty, this.hackDifficulty - ns.weakenAnalyze(threads, cores));
        const securityChange = this.hackDifficulty - this.prevDifficulty;
        // Construct job
        const job = {
            task: 'weaken',
            args: [server.hostname, { threads: threads }],
            threads: threads,
            duration: duration,
            change: { security: securityChange, moneyMult: 1, money: 0, playerMoney: 0 },
            result: this.copy(),
        };
        return job;
    }
    /**
     * Construct a batch of 'grow' and 'weaken' jobs that will bring the server
     * to a ready state (maximum money and minimum security).
     * @param {Object} params
     * @param {number} [params.maxThreadsPerJob=512]
     * @param {number} [params.prepMargin=0.5] - amount of security above minimum to allow between jobs
     * @param {boolean} [params.naiveSplit=false] - whether to place all jobs of the same type together
     * @param {boolean} [params.growStock] - whether to manipulate stock performance for 'grow' actions
     * @param {number} [params.cores=1]
     * @retruns {Batch}
     */
    planPrepBatch(params) {
        var _a;
        const defaults = {
            maxThreadsPerJob: 512,
            prepMargin: 0.5,
            naiveSplit: false,
            growStock: ((_a = this.getStockInfo()) === null || _a === void 0 ? void 0 : _a.netShares) >= 0,
            cores: 1
        };
        params = Object.assign({}, defaults, params);
        const { maxThreadsPerJob, prepMargin, naiveSplit, growStock, cores } = params;
        const batch = new Batch();
        while (naiveSplit && this.hackDifficulty > this.minDifficulty + prepMargin) {
            batch.push(this.planWeaken(maxThreadsPerJob, cores));
        }
        while (this.moneyAvailable < this.moneyMax) {
            while (!naiveSplit && this.hackDifficulty > this.minDifficulty + prepMargin) {
                batch.push(this.planWeaken(maxThreadsPerJob, cores));
            }
            batch.push(this.planGrow(maxThreadsPerJob, cores, growStock));
        }
        while (this.hackDifficulty > this.minDifficulty) {
            batch.push(this.planWeaken(maxThreadsPerJob, cores));
        }
        this.prepDifficulty = this.hackDifficulty; // This isn't true until some delay has passed. Should that delay be represented in the batch data structure?
        return batch;
    }
    /**
     * Construct a Batch of jobs that will hack a server and then return it to a ready state.
     * Higher moneyPercent or hackMargin will result in more threads per job.
     * @param {Object} params - Parameters for jobs to add to the batch. Additional values will be passed to planPrepBatch.
     * @param {number}
     * @param {number} [params.maxThreadsPerJob=512]
     * @param {number} [params.prepMargin=0.5] - amount of security above minimum to allow between jobs
     * @param {boolean} [params.naiveSplit=false] - whether to place all jobs of the same type together
     * @param {boolean} [params.hackStock] - whether to manipulate stock performance for 'hack' actions
     * @param {boolean} [params.growStock] - whether to manipulate stock performance for 'grow' actions
     * @param {number} [params.cores=1]
     * @retruns {Batch}
     */
    planHackingBatch(params) {
        var _a, _b;
        const defaults = {
            moneyPercent: 0.05,
            maxThreadsPerJob: 512,
            hackMargin: 0.25,
            hackStock: ((_a = this.getStockInfo()) === null || _a === void 0 ? void 0 : _a.netShares) < 0,
            growStock: ((_b = this.getStockInfo()) === null || _b === void 0 ? void 0 : _b.netShares) >= 0,
            cores: 1
        };
        params = Object.assign({}, defaults, params);
        const { moneyPercent, maxThreadsPerJob, hackMargin, hackStock, growStock, cores } = params;
        const batch = new Batch();
        batch.push(this.planHack(moneyPercent, maxThreadsPerJob, hackStock));
        while (this.hackDifficulty < this.minDifficulty + hackMargin) {
            batch.push(this.planGrow(maxThreadsPerJob, cores, growStock));
            batch.push(this.planHack(moneyPercent, maxThreadsPerJob, hackStock));
        }
        batch.push(...this.planPrepBatch(params));
        return batch;
    }
    /**
     * Calculate the amount of time it will take to bring a server from its current state
     * to a ready state (max money and min security).
     * @param {Object} params - Parameters for jobs to add to the batch. Additional values will be passed to planHackBatch and planPrepBatch.
     * @param {number} [params.tDelta=100] - milliseconds between job completions
     * @returns {number} ms
     */
    estimatePrepTime(params) {
        const defaults = {
            tDelta: 100
        };
        params = Object.assign({}, defaults, params);
        const { tDelta } = params;
        const batch = this.planPrepBatch(params);
        return batch.totalDuration(tDelta);
    }
    /**
     * Calculate metrics of a hacking batch.
     * @param {Object} params - Parameters for jobs to add to the batch. Additional values will be passed to planHackBatch and planPrepBatch.
     * @param {number} [params.maxTotalRam=16384] - GB of ram to use for multiple batches
     * @param {number} [params.tDelta=100] - milliseconds between job completions
     * @param {boolean} [params.reserveRam=false] - whether to plan based on peak RAM usage instead of average RAM usage
     * @returns {BatchCycle} Details of the planned batch cycle
     */
    planBatchCycle(params) {
        const defaults = {
            moneyPercent: 0.05,
            maxTotalRam: 16384,
            maxThreadsPerJob: 512,
            hackMargin: 0.25,
            prepMargin: 0.5,
            naiveSplit: false,
            cores: 1,
            tDelta: 100,
            reserveRam: false
        };
        params = Object.assign({}, defaults, params);
        const { maxTotalRam, tDelta, reserveRam } = params;
        const server = this.preppedCopy(Math.max(0, params.hackMargin, params.prepMargin));
        const batch = server.planHackingBatch(params);
        const moneyPerBatch = batch.moneyTaken();
        const period = batch.totalDuration(tDelta);
        const numBatchesAtOnce = batch.maxBatchesAtOnce(maxTotalRam, tDelta, reserveRam);
        const timeBetweenStarts = period / numBatchesAtOnce;
        const totalMoney = moneyPerBatch * numBatchesAtOnce;
        const moneyPerSec = totalMoney / (period / 1000);
        // const totalThreads = numBatchesAtOnce * batch.avgThreads(tDelta);
        // const moneyPerSecPerThread = moneyPerSec / totalThreads;
        const peakRam = numBatchesAtOnce * batch.peakRam();
        const avgRam = numBatchesAtOnce * batch.avgRam(tDelta);
        const moneyPerSecPerGB = moneyPerSec / avgRam;
        const maxThreads = batch.maxThreads();
        const condition = `${batch.moneySummary()} ${batch.summary()}`;
        const batchCycle = {
            condition,
            batch,
            params,
            period,
            numBatchesAtOnce,
            timeBetweenStarts,
            peakRam,
            avgRam,
            moneyPerSec,
            moneyPerSecPerGB,
            maxThreadsPerJob: maxThreads
        };
        return batchCycle;
    }
    /**
     * Sweep parameters to survey various types of batches.
     * @param {Object} params
     * @param {number} [params.maxThreadsPerJob=512] - maximum amount of threads to use for a single job
     * @param {number} [params.maxTotalRam=16384] - maximum amount of ram to use for an entire batch cycle
     * @param {number} [params.tdelta=100] - milliseconds between actions
     * @returns {BatchCycle[]} - list of ideal cycles for each set of parameters
     */
    *sweepParameters(params) {
        const defaults = {
            maxThreadsPerJob: 512,
            maxTotalRam: 16384,
            tDelta: 100
        };
        params = Object.assign({}, defaults, params);
        const estimates = [];
        for (const moneyPercent of range(1 / 40, 1, 1 / 40)) {
            for (const hackMargin of [0, 0.25]) {
                for (const prepMargin of [0, 0.5]) {
                    for (const naiveSplit of [false]) {
                        const batchParams = Object.assign(Object.assign({}, params), { moneyPercent, hackMargin, prepMargin, naiveSplit });
                        const batchCycle = this.planBatchCycle(batchParams);
                        yield batchCycle;
                    }
                }
            }
        }
        return estimates;
    }
    mostProfitableParamsSync(params) {
        const estimates = [...this.sweepParameters(params)];
        const bestEstimate = estimates.sort((a, b) => (b.moneyPerSec - a.moneyPerSec))[0];
        return bestEstimate.params;
    }
    async mostProfitableParams(params) {
        const estimates = [];
        let i = 0;
        for (const estimate of this.sweepParameters(params)) {
            estimates.push(estimate);
            if (++i % 10 == 0) {
                await sleep(1);
            }
        }
        const bestEstimate = estimates.sort((a, b) => (b.moneyPerSec - a.moneyPerSec))[0];
        return bestEstimate.params;
    }
}
export class HackPlanner extends ServerList {
    constructor() {
        super(...arguments);
        this.ServerClass = HackableServer;
    }
    mostProfitableServers(params, hostnames = []) {
        const { ns } = this;
        const plans = [];
        let servers = this;
        console.log('blah2');
        if (hostnames.length > 0) {
            servers = hostnames.map((hostname) => this.loadServer(hostname));
        }
        else {
            servers = this.getHackableServers(ns.getPlayer());
        }
        for (let server of servers) {
            const bestParams = server.mostProfitableParamsSync(params);
            const batchCycle = server.planBatchCycle(bestParams);
            batchCycle.prepTime = server.estimatePrepTime(params);
            const ONE_HOUR = 60 * 60 * 1000;
            const cycleTimeInNextHour = Math.max(1000, ONE_HOUR - batchCycle.prepTime);
            batchCycle.moneyInNextHour = batchCycle.moneyPerSec * cycleTimeInNextHour;
            batchCycle.server = server;
            batchCycle.totalRamBytes = batchCycle.peakRam * 1e9;
            server.reload();
            plans.push(batchCycle);
        }
        const bestPlans = plans.sort((a, b) => (b.moneyInNextHour - a.moneyInNextHour));
        return bestPlans;
    }
    reportMostProfitableServers(params) {
        const { ns } = this;
        const columns = [
            { header: "Hostname", field: "server.hostname", width: 18, align: "left" },
            { header: "Parameters", field: "condition", width: 16, align: "left", truncate: true },
            { header: "Prep Time", field: "prepTime", format: drawTable.time },
            { header: "RAM Used", field: "totalRamBytes", format: ns.nFormat, formatArgs: ["0.0 b"] },
            { header: "  $ / sec", field: "moneyPerSec", format: ns.nFormat, formatArgs: ["$0.0a"] },
            // {header: "Max threads/job", field: "maxThreadsPerJob"}
            // {header: "$/sec/GB", field: "moneyPerSecPerGB", format: ns.nFormat, formatArgs: ["$0.00a"]},
        ];
        const rows = this.mostProfitableServers(params);
        eval("window").mostProfitableServers = rows;
        return drawTable(`Most Profitable Servers to Hack (${ns.nFormat(params.maxTotalRam * 1e9, "0.0 b")} total RAM)`, columns, rows);
    }
    reportBatchLengthComparison(server, params) {
        var _a;
        const { ns } = this;
        server = new HackableServer(ns, server);
        const columns = [
            { header: "Condition", field: "condition", width: 28, align: "left", truncate: true },
            // {header: "Duration", field: "duration", format: drawTable.time},
            { header: "Batches", field: "numBatchesAtOnce" },
            { header: "Max t", field: "maxThreadsPerJob" },
            { header: "RAM Used", field: "totalRamBytes", format: ns.nFormat, formatArgs: ["0.0 b"] },
            { header: "  $ / sec", field: "moneyPerSec", format: ns.nFormat, formatArgs: ["$0.0a"] },
            // {header: "$/sec/GB", field: "moneyPerSecPerGB", format: ns.nFormat, formatArgs: ["$0.00a"]},
        ];
        const estimates = server.sweepParameters(params);
        const estimatesByMoneyPct = {};
        for (const estimate of estimates) {
            estimate.totalRamBytes = estimate.peakRam * 1e9;
            estimatesByMoneyPct[_a = estimate.params.moneyPercent] || (estimatesByMoneyPct[_a] = []);
            estimatesByMoneyPct[estimate.params.moneyPercent].push(estimate);
        }
        const bestEstimates = {};
        for (const moneyPercent of Object.keys(estimatesByMoneyPct)) {
            const estimates = estimatesByMoneyPct[moneyPercent].sort((a, b) => (b.moneyPerSec - a.moneyPerSec));
            for (const estimate of estimates) {
                bestEstimates[estimate.condition] = estimate;
                break;
            }
        }
        return drawTable(`Comparison of batches (${ns.nFormat(params.maxTotalRam * 1e9, "0.0 b")} total RAM, max ${params.maxThreadsPerJob} threads per job)`, columns, Object.values(bestEstimates));
    }
}
