var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
export class CachedData {
    constructor(ns, key, options) {
        this.ns = ns;
        this.options = options;
        this._key = "";
        this._data = null;
        this.lastLSUpdate = 0;
        this.lastFSUpdate = 0;
        this.lastUpdate = 0;
        this.key = key;
        if (options.data) {
            this.data = options.data;
        }
    }
    load() {
        let stored = null;
        if (this.options.localStorage === true && this.options.file) {
            let lsStored = this.loadFromLocalStorage();
            let fsStored = this.loadFromFile();
            stored = (lsStored.lastUpdate > fsStored.lastUpdate ? lsStored : fsStored);
        }
        else if (this.options.localStorage === true) {
            stored = this.loadFromLocalStorage();
        }
        else if (this.options.file) {
            stored = this.loadFromFile();
        }
        if (stored !== null) {
            this._data = stored.data;
        }
        return this;
    }
    async sync() {
        let stored = null;
        if (this.options.localStorage === true && this.options.file) {
            let lsStored = this.loadFromLocalStorage();
            let fsStored = this.loadFromFile();
            stored = (lsStored.lastUpdate > fsStored.lastUpdate ? lsStored : fsStored);
        }
        else if (this.options.localStorage === true) {
            stored = this.loadFromLocalStorage();
        }
        else if (this.options.file) {
            stored = this.loadFromFile();
        }
        if (stored !== null) {
            if (this.lastUpdate > stored.lastUpdate) {
                await this.save();
            }
            else {
                this._data = stored.data;
            }
        }
        return this;
    }
    loadFromLocalStorage() {
        let item = localStorage.getItem(this.key);
        return item ? JSON.parse(item) : undefined;
    }
    loadFromFile() {
        let item = this.ns.read(`${this.options.file}.txt`);
        return item ? JSON.parse(item) : undefined;
    }
    get key() {
        return this._key;
    }
    set key(key) {
        this._key = key;
    }
    get data() {
        return this._data;
    }
    async updateData(data) {
        let oldData = this._data;
        this._data = data;
        await this.onChange(oldData, data);
        this.lastUpdate = Date.now();
    }
    set data(data) {
        let oldData = this._data;
        this._data = data;
        this.onChange(oldData, data).then(() => {
            this.lastUpdate = Date.now();
        }).catch((err) => {
            this.ns.tprint(err);
        });
    }
    async save() {
        if (this.options.localStorage === true) {
            localStorage.setItem(this.key, JSON.stringify({ data: this.data, lastUpdate: Date.now() }));
        }
        if (this.options.file) {
            await this.ns.write(`${this.options.file}.txt`, [JSON.stringify({
                    data: this.data,
                    lastUpdate: Date.now()
                })], 'w');
        }
    }
    async onChange(oldData, newData) {
        let now = Date.now();
        if (this.options.localStorage === true) {
            if (now > this.lastLSUpdate + this.options.localStorageFrequency) {
                this.lastLSUpdate = now;
                localStorage.setItem(this.key, JSON.stringify({ data: newData, lastUpdate: this.lastLSUpdate }));
            }
        }
        if (this.options.file) {
            if (now > this.lastFSUpdate + this.options.fileFrequency) {
                this.lastFSUpdate = now;
                await this.ns.write(`${this.options.file}.txt`, [JSON.stringify({ data: newData, lastUpdate: this.lastFSUpdate })], 'w');
            }
        }
    }
}
export const Cities = [
    "Aevum",
    "Chongqing",
    "Sector-12",
    "New Tokyo",
    "Ishima",
    "Volhaven"
];
export const AllMaterials = [
    "Water",
    "Energy",
    "Food",
    "Plants",
    "Metal",
    "Hardware",
    "Chemicals",
    "Drugs",
    "Robots",
    "AI Cores",
    "Real Estate",
];
export function addTerminalLine() {
    let terminal = document.getElementById("terminal");
    if (terminal) {
        terminal.insertAdjacentHTML('beforeend', `<li class="jss112 MuiListItem-root MuiListItem-gutters MuiListItem-padding css-1578zj2"><p class="jss117 MuiTypography-root MuiTypography-body1 css-cxl1tz">whatever <span style="color:red">custom</span> html</p></li>`);
    }
}
/*
  Optained from https://www.reddit.com/r/Bitburner/comments/shzcoi/guide_rendering_custom_tail_windows_with_react/ and modified
 */
export function getColorScale(v) {
    return `hsl(${Math.max(0, Math.min(1, v)) * 130}, 100%, 50%)`;
}
export const toolbarStyles = {
    lineHeight: '30px',
    alignItems: 'center',
    display: 'flex',
    gap: 16,
    margin: 8,
};
export function getCommandLine(ns) {
    return ns.getScriptName() + (ns.args.length > 0 ? ' ' + ns.args.join(' ') : '');
}
export function getTailModal(ns) {
    const commandLine = getCommandLine(ns);
    const modals = eval('document').querySelectorAll('.drag > h6');
    const tailTitleEl = Array.from(modals).find(x => x.textContent.includes(commandLine));
    // return tailTitleEl === null || tailTitleEl === void 0 ? void 0 : tailTitleEl.parentElement.parentElement.nextSibling;
    // return tailTitleEl === null || tailTitleEl === void 0 ? void 0 : tailTitleEl.parentElement.parentElement.childNodes[1].firstElementChild;
    // return tailTitleEl?.parentElement!.parentElement!.nextSibling;
    return (tailTitleEl === null || tailTitleEl === void 0 ? void 0 : tailTitleEl.parentElement.parentElement.childNodes[1]).firstElementChild;
}
/**
 * Creates a custom container inside a tail modal to use for rendering custom DOM.
 * If the container has already been created, the existing container will be returned.
 */
export function getCustomModalContainer(ns) {
    const id = getCommandLine(ns).replace(/[^\w\.]/g, '_');
    let containerEl = eval('document').getElementById(id);
    if (!containerEl) {
        const modalEl = getTailModal(ns);
        if (!modalEl) {
            return null;
        }
        containerEl = eval('document').createElement('div');
        if (containerEl) {
            containerEl.id = id;
            containerEl.style.fontFamily = '"Lucida Console", "Lucida Sans Unicode", "Fira Mono", Consolas, "Courier New", Courier, monospace, "Times New Roman"';
            containerEl.style.fontWeight = '400';
            containerEl.style.position = 'absolute';
            containerEl.style.overflow = 'auto';
            containerEl.style.left = '0';
            containerEl.style.right = '0';
            containerEl.style.top = '34px';
            containerEl.style.bottom = '0';
            containerEl.style.background = 'black';
            containerEl.style.color = 'rgb(0, 204, 0)';
            modalEl.insertBefore(containerEl, modalEl.firstChild);
        }
    }
    return containerEl;
}
/**
 * Render a custom modal with react
 *
 * @example
 * renderCustomModal(ns,
 *   <div>
 *     Hello world!
 *   </div>
 * );
 */
export function renderCustomModal(ns, element) {
    const container = getCustomModalContainer(ns);
    if (!container) {
        return;
    }
    eval('window').ReactDOM.render(element, container);
}
/**
 * Simple event queue for event handlers which need to call netscript functions
 *
 * @example
 * // Render custom modal
 * renderCustomModal(ns,
 *   <div>
 *     <button onClick={eventQueue.wrap(event => ns.killall())}>Kill all scripts</button>
 *   </div>
 * );
 * // Execute all events which have been triggered since last invocation of executeEvents
 * await eventQueue.executeEvents();
 */
export class EventHandlerQueue {
    constructor() {
        this.queue = [];
    }
    wrap(fn) {
        return ((...args) => {
            if (args[0] && typeof args[0] === 'object' && typeof args[0].persist === 'function') {
                args[0].persist();
            }
            this.queue.push(() => fn(...args));
        });
    }
    async executeEvents() {
        var e_1, _a;
        const events = this.queue;
        this.queue = [];
        try {
            for (var events_1 = __asyncValues(events), events_1_1; events_1_1 = await events_1.next(), !events_1_1.done;) {
                const event = events_1_1.value;
                await event();
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (events_1_1 && !events_1_1.done && (_a = events_1.return)) await _a.call(events_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
    }
}
/**
 * Template-String function which does nothing else than concatenating all parts.
 * This function can be used in editors like VSCode to get syntax highlighting & more for inline CSS strings
 *
 * @example
 *
 * <style children={css`
 *     .myClass {
 *         color: red;
 *     }
 * `} />
 */
export function css(parts, ...params) {
    let result = parts[0];
    for (let i = 1; i < parts.length; i++) {
        result += params[i - 1] + parts[i];
    }
    return result;
}
/*
columns: {width, header, field, format, align, truncate}
rows: obj with each [field]
*/
export function drawTable(title = undefined, columns, ...rowSets) {
    let totalWidth = 3 * columns.length + 1;
    for (const col of columns) {
        col.width || (col.width = col.header.length);
        totalWidth += col.width;
    }
    let lines = [];
    if (title) {
        lines.push(' ');
        lines.push(pad(title, totalWidth, ' ', 'center'));
    }
    lines.push(drawHR(columns, ['┌', '┬', '─', '┐']));
    lines.push('│ ' + columns.map((col) => pad(col.header || col.field, (col.width ? col.width : 2), ' ', col.align || 'left')).join(' │ ') + ' │');
    for (const rows of rowSets) {
        lines.push(drawHR(columns, ['├', '┼', '─', '┤']));
        for (const row of rows) {
            const values = [];
            for (const col of columns) {
                let val = getField(row, col.field);
                if (Array.isArray(col.field)) {
                    val = col.field.map((f) => getField(row, f));
                }
                if (Array.isArray(col.format)) {
                    const vals = (val || []).map((v) => (col.format[0](v, ...(col.formatArgs || []))));
                    val = formatFraction(vals, col.itemWidth);
                }
                else if (typeof (col.format) == 'function') {
                    if (!Number.isNaN(val)) {
                        val = col.format(val, ...(col.formatArgs || []));
                    }
                }
                val = pad(`${val || ''}`, (col.width ? col.width : 2), ' ', col.align || 'right');
                if (col.truncate && val.length > (col.width ? col.width : 2)) {
                    val = val.substring(0, (col.width ? col.width : 2) - 1) + "…";
                }
                values.push(val);
            }
            lines.push('│ ' + values.join(' │ ') + ' │');
        }
    }
    lines.push(drawHR(columns, ['└', '┴', '─', '┘']));
    return lines.join('\n');
}
function drawHR(columns, glyphs = ['└', '┴', '─', '┘']) {
    let line = glyphs[0];
    const segments = [];
    for (const col of columns) {
        const segment = pad('', (col.width ? col.width + 2 : 2), glyphs[2]);
        segments.push(segment);
    }
    line = glyphs[0] + segments.join(glyphs[1]) + glyphs[3];
    return line;
}
function pad(str, length, filler = ' ', align = 'right') {
    if (align == 'right') {
        while (str.length < length) {
            str = filler + str;
        }
    }
    else if (align == 'left') {
        while (str.length < length) {
            str = str + filler;
        }
    }
    else {
        while (str.length < length) {
            str = str + filler;
            if (str.length < length) {
                str = filler + str;
            }
        }
    }
    return str;
}
function getField(obj, fieldName) {
    let cursor = obj;
    for (const part of `${fieldName || ''}`.split('.')) {
        cursor = cursor[part];
    }
    return cursor;
}
export function formatTime(timeMS, precision = 0) {
    if (!timeMS) {
        return '';
    }
    let sign = '';
    if (timeMS < 0) {
        sign = '-';
        timeMS = Math.abs(timeMS);
    }
    const d = new Date(2000, 1, 1, 0, 0, timeMS / 1000);
    let timeStr = d.toTimeString().slice(0, 8);
    if (timeMS >= 60 * 60 * 1000) {
        timeStr = timeStr.slice(0, 8);
    }
    else if (timeMS >= 10 * 60 * 1000) {
        timeStr = timeStr.slice(3, 8);
    }
    else {
        timeStr = timeStr.slice(4, 8);
    }
    if (precision > 0) {
        let msStr = (timeMS / 1000 - Math.floor(timeMS / 1000)).toFixed(precision);
        timeStr += msStr.substring(1);
    }
    return sign + timeStr;
}
drawTable.time = formatTime;
export function formatFraction(fraction, itemWidth = 0) {
    const values = fraction.filter((val) => !!val).map((val) => pad(val, itemWidth));
    return values.join(" / ");
}
drawTable.fraction = formatFraction;
export function* range(start, stop, step) {
    if (step <= 0 || stop < start) {
        return;
    }
    let i = start;
    while (i < stop) {
        yield i;
        i += step;
    }
}
export function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
