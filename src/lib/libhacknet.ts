import {NodeStats, NS} from "../../index";

export function getHacknetNodes(): NodeStats[] | false {
  if (localStorage.hacknetNodes) {
    return JSON.parse(localStorage.hacknetNodes);
  } else {
    return false;
  }
}
export function updateHacknetNodes(ns: NS) {
  let nodes: NodeStats[] = [];
  const numNodes = ns.hacknet.numNodes();
  for (let i = 0 ; i < numNodes ; i++) {
    nodes.push(ns.hacknet.getNodeStats(i));
  }
  localStorage.hacknetNodes = JSON.stringify(nodes);
}
