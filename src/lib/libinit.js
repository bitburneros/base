export function getInitScripts(ns) {
    return ns.ls('home', '/etc/init_d/');
}
export function startInitScripts(ns, verbose = false) {
    const initFiles = getInitScripts(ns);
    for (const initFile of initFiles) {
        if (verbose) {
            ns.tprint(`INFO: Starting Init Script ${initFile}`);
        }
        if (ns.isRunning(initFile, 'home')) {
            ns.scriptKill(initFile, 'home');
        }
        const pid = ns.run(initFile, 1, 'start');
        if (pid === 0) {
            if (verbose) {
                ns.tprint(`ERROR: Unable to start Init Script ${initFile}`);
            }
        }
        else {
            if (verbose) {
                ns.tprint(`INFO: Starting Init Script ${initFile} with PID ${pid}`);
            }
        }
    }
}
export function stopInitScripts(ns, verbose = false) {
    const initFiles = getInitScripts(ns);
    for (const initFile of initFiles) {
        if (verbose) {
            ns.tprint(`INFO Stopping Init Script ${initFile}`);
        }
        ns.run(initFile, 1, 'stop');
    }
}
export function getInitScriptsStatus(ns, verbose = false) {
    const initFiles = getInitScripts(ns);
    for (const initFile of initFiles) {
        if (verbose) {
            ns.tprint(`INFO Getting Init Script ${initFile} status`);
        }
        ns.run(initFile, 1, 'status');
    }
}
export function getBootScripts(ns) {
    return ns.ls('home', '/boot/');
}
export function startBootScripts(ns, verbose = false) {
    const bootScripts = getBootScripts(ns);
    for (const bootScript of bootScripts) {
        if (verbose) {
            ns.tprint(`INFO: Starting Boot Script ${bootScript}`);
        }
        if (ns.isRunning(bootScript, 'home')) {
            ns.scriptKill(bootScript, 'home');
        }
        const pid = ns.run(bootScript, 1, 'start');
        if (pid === 0) {
            if (verbose) {
                ns.tprint(`ERROR: Unable to start Boot Script ${bootScript}`);
            }
        }
        else {
            if (verbose) {
                ns.tprint(`INFO: Starting Boot Script ${bootScript} with PID ${pid}`);
            }
        }
    }
}
export function stopBootScripts(ns, verbose = false) {
    const bootScripts = getBootScripts(ns);
    for (const bootScript of bootScripts) {
        if (verbose) {
            ns.tprint(`INFO Stopping Boot Script ${bootScript}`);
        }
        ns.run(bootScript, 1, 'stop');
    }
}
export function getBootScriptsStatus(ns, verbose = false) {
    const bootScripts = getBootScripts(ns);
    for (const bootScript of bootScripts) {
        if (verbose) {
            ns.tprint(`INFO Getting Boot Script ${bootScript} status`);
        }
        ns.run(bootScript, 1, 'status');
    }
}
