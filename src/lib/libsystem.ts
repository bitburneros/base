import {getNetwork, System} from "lib/libnetwork";
import {getPlayer} from "lib/libplayer";

export function getSystem() {
  if (localStorage.system) {
    return JSON.parse(localStorage.system);
  } else {
    return false;
  }
}
export function updateSystem() {
  let player = getPlayer();
  let network = getNetwork();
  if (player && network) {
    let mySystem: System = {
      cpus: network.cpuCores,
      ram: network.maxRam,
      maxCPUs: 8,
      maxRam: 1073741824,
      canUpgradeCPUs: false,
      canUpgradeRam: false
    };
    mySystem.canUpgradeCPUs = mySystem.cpus !== mySystem.maxCPUs;
    mySystem.canUpgradeRam = mySystem.ram !== mySystem.maxRam;
    localStorage.system = JSON.stringify(mySystem);
  }
}
