import {CorporationInfo, NS, Player} from "../../index";


interface IIndustryMap<T> {
  [key: string]: T | undefined;
  Energy: T;
  Utilities: T;
  Agriculture: T;
  Fishing: T;
  Mining: T;
  Food: T;
  Tobacco: T;
  Chemical: T;
  Pharmaceutical: T;
  Computer: T;
  Robotics: T;
  Software: T;
  Healthcare: T;
  RealEstate: T;
}
// Map of official names for each Industry
export const Industries: IIndustryMap<string> = {
  Energy: "Energy",
  Utilities: "Water Utilities",
  Agriculture: "Agriculture",
  Fishing: "Fishing",
  Mining: "Mining",
  Food: "Food",
  Tobacco: "Tobacco",
  Chemical: "Chemical",
  Pharmaceutical: "Pharmaceutical",
  Computer: "Computer Hardware",
  Robotics: "Robotics",
  Software: "Software",
  Healthcare: "Healthcare",
  RealEstate: "RealEstate",
};
// Map of how much money it takes to start each industry
export const IndustryStartingCosts: IIndustryMap<number> = {
  Energy: 225e9,
  Utilities: 150e9,
  Agriculture: 40e9,
  Fishing: 80e9,
  Mining: 300e9,
  Food: 10e9,
  Tobacco: 20e9,
  Chemical: 70e9,
  Pharmaceutical: 200e9,
  Computer: 500e9,
  Robotics: 1e12,
  Software: 25e9,
  Healthcare: 750e9,
  RealEstate: 600e9,
};
// Map of industry descriptions
export const IndustryDescriptions: IIndustryMap<string> = {
  Energy: "Engage in the production and distribution of energy.",
  Utilities: "Distribute water and provide wastewater services.",
  Agriculture: "Cultivate crops and breed livestock to produce food.",
  Fishing: "Produce food through the breeding and processing of fish and fish products.",
  Mining: "Extract and process metals from the earth.",
  Food: "Create your own restaurants all around the world.",
  Tobacco: "Create and distribute tobacco and tobacco-related products.",
  Chemical: "Produce industrial chemicals.",
  Pharmaceutical: "Discover, develop, and create new pharmaceutical drugs.",
  Computer: "Develop and manufacture new computer hardware and networking infrastructures.",
  Robotics: "Develop and create robots.",
  Software: "Develop computer software and create AI Cores.",
  Healthcare: "Create and manage hospitals.",
  RealEstate: "Develop and manage real estate properties.",
};

export const CorporationUnlockUpgrades = [
  {
    index: 0,
    cost: 20e9,
    name: "Export",
    description: "Develop infrastructure to export your materials to your other facilities. This allows you to move materials around between different divisions and cities."
  },
  {
    index: 1,
    cost: 25e9,
    name: "Smart Supply",
    description: "Use advanced AI to anticipate your supply needs. This allows you to purchase exactly however many materials you need for production."
  },
  {
    index: 2,
    cost: 5e9,
    name: "Market Research - Demand",
    description: "Mine and analyze market data to determine the demand of all resources. The demand attribute, which affects sales, will be displayed for every material and product."
  },
  {
    index: 3,
    cost:5e9 ,
    name: "Market Data - Competition",
    description: "Mine and analyze market data to determine how much competition there is on the market for all resources. The competition attribute, which affects sales, will be displayed for every material and product."
  },
  {
    index: 4,
    cost: 10e9,
    name: "VeChain",
    description: "Use AI and blockchain technology to identify where you can improve your supply chain systems. This upgrade will allow you to view a wide array of useful statistics about your Corporation."
  },
  {
    index: 5,
    cost: 500e12,
    name: "Shady Accounting",
    description: "Utilize unscrupulous accounting practices and pay off government officials to save money on taxes. This reduces the dividend tax rate by 5%."
  },
  {
    index: 6,
    cost: 2e15,
    name: "Government Partnership",
    description: "Help national governments further their agendas in exchange for lowered taxes. This reduces the dividend tax rate by 10%"
  },
  {
    index: 7,
    cost: 50e9,
    name: "Warehouse API",
    description: "Enables the warehouse API."
  },
  {
    index: 8,
    cost: 50e9,
    name: "Office API",
    description: "Enables the office API."
  },
];

export type CorporationUpgrade = [number, number, number, string, string];
// Corporation Upgrades
// Upgrades for entire corporation, levelable upgrades
// The data structure is an array with the following format
//  [index in Corporation upgrades array, base price, price mult, benefit mult (additive), name, desc]
export const CorporationUpgrades: CorporationUpgrade[] = [
  //Smart factories, increases production
  [
    2e9,
    1.06,
    0.03,
    "Smart Factories",
    "Advanced AI automatically optimizes the operation and productivity " +
    "of factories. Each level of this upgrade increases your global production by 3% (additive).",
  ],
  //Smart warehouses, increases storage size
  [
    2e9,
    1.06,
    0.1,
    "Smart Storage",
    "Advanced AI automatically optimizes your warehouse storage methods. " +
    "Each level of this upgrade increases your global warehouse storage size by 10% (additive).",
  ],
  //Advertise through dreams, passive popularity/ awareness gain
  [
    4e9,
    1.1,
    0.001,
    "DreamSense",
    "Use DreamSense LCC Technologies to advertise your corporation " +
    "to consumers through their dreams. Each level of this upgrade provides a passive " +
    "increase in awareness of all of your companies (divisions) by 0.004 / market cycle," +
    "and in popularity by 0.001 / market cycle. A market cycle is approximately " +
    "15 seconds.",
  ],
  //Makes advertising more effective
  [
    4e9,
    1.5,
    0.005,
    "Wilson Analytics",
    "Purchase data and analysis from Wilson, a marketing research " +
    "firm. Each level of this upgrades increases the effectiveness of your " +
    "advertising by 0.5% (additive).",
  ],
  //Augmentation for employees, increases cre
  [
    1e9,
    1.06,
    0.1,
    "Nuoptimal Nootropic Injector Implants",
    "Purchase the Nuoptimal Nootropic " +
    "Injector augmentation for your employees. Each level of this upgrade " +
    "globally increases the creativity of your employees by 10% (additive).",
  ],
  //Augmentation for employees, increases cha
  [
    1e9,
    1.06,
    0.1,
    "Speech Processor Implants",
    "Purchase the Speech Processor augmentation for your employees. " +
    "Each level of this upgrade globally increases the charisma of your employees by 10% (additive).",
  ],
  //Augmentation for employees, increases int
  [
    1e9,
    1.06,
    0.1,
    "Neural Accelerators",
    "Purchase the Neural Accelerator augmentation for your employees. " +
    "Each level of this upgrade globally increases the intelligence of your employees " +
    "by 10% (additive).",
  ],
  //Augmentation for employees, increases eff
  [
    1e9,
    1.06,
    0.1,
    "FocusWires",
    "Purchase the FocusWire augmentation for your employees. Each level " +
    "of this upgrade globally increases the efficiency of your employees by 10% (additive).",
  ],
  //Improves sales of materials/products
  [
    1e9,
    1.07,
    0.01,
    "ABC SalesBots",
    "Always Be Closing. Purchase these robotic salesmen to increase the amount of " +
    "materials and products you sell. Each level of this upgrade globally increases your sales " +
    "by 1% (additive).",
  ],
  //Improves scientific research rate
  [
    5e9,
    1.07,
    0.05,
    "Project Insight",
    "Purchase 'Project Insight', a R&D service provided by the secretive " +
    "Fulcrum Technologies. Each level of this upgrade globally increases the amount of " +
    "Scientific Research you produce by 5% (additive).",
  ],
];

export function getCorporation(): CorporationInfo | false {
  if (localStorage.corporation) {
    return JSON.parse(localStorage.corporation);
  } else {
    return false;
  }
}
export function updateCorporation(ns: NS) {
  const sourceFiles = JSON.parse(localStorage.sourceFiles);
  const player: Player = JSON.parse(localStorage.player);
  if (player.bitNodeN === 3 || sourceFiles[3] !== undefined) {
    try {
      localStorage.corporation = JSON.stringify(ns.corporation.getCorporation());
    } catch (e) {
      if (localStorage.corporation) {
        localStorage.removeItem('corporation');
      }
    }
  }
}
