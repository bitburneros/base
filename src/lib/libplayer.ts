import {NS, Player} from "../../index";

export function getPlayer(): Player | false {
  if (localStorage.player) {
    return JSON.parse(localStorage.player);
  } else {
    return false;
  }
}
export function updatePlayer(ns: NS) {
  localStorage.player = JSON.stringify(ns.getPlayer());
}
