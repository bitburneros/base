import { getPlayer } from "lib/libplayer";
export function getSourceFiles() {
    if (localStorage.sourceFiles) {
        return JSON.parse(localStorage.sourceFiles);
    }
    else {
        return false;
    }
}
export function getBitNodeMultipliers() {
    if (localStorage.sourceFiles) {
        return JSON.parse(localStorage.sourceFiles);
    }
    else {
        return false;
    }
}
export function updateSourceFiles(ns) {
    let sourceFiles = ns.getOwnedSourceFiles();
    let sourceFilesTree = {};
    sourceFiles.forEach((sourceFile) => {
        sourceFilesTree[sourceFile.n] = sourceFile.lvl;
    });
    const player = getPlayer();
    if (player && !sourceFilesTree[player.bitNodeN]) {
        sourceFilesTree[player.bitNodeN] = 0;
    }
    localStorage.sourceFiles = JSON.stringify(sourceFilesTree);
}
export function updateBitNodeMultipliers(ns) {
    const sourceFiles = getSourceFiles();
    if (sourceFiles && sourceFiles[5]) {
        localStorage.bitNodeMultipliers = JSON.stringify(ns.getBitNodeMultipliers());
    }
}
