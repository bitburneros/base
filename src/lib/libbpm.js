export async function install(ns, pkg) {
    if (ns.getHostname() !== "home") {
        throw new Error("Run the script from home");
    }
    let packages = {};
    if (localStorage.bpmPackages) {
        packages = JSON.parse(localStorage.bpmPackages);
    }
    // const baseUrl = 'https://gitlab.com/b2992/second/-/raw/main/';
    if (await ns.wget(`${pkg}/package.json`, '/tmp/package.txt')) {
        let pkgInfo = JSON.parse(ns.read('/tmp/package.txt'));
        ns.rm('/tmp/package.txt');
        ns.tprint(`INFO Getting ${pkgInfo.name} files list.`);
        if (await ns.wget(`${pkg}/files.json`, '/tmp/files.txt')) {
            let files = JSON.parse(ns.read('/tmp/files.txt'));
            ns.rm('/tmp/files.txt');
            ns.tprint(`INFO Downloading ${pkgInfo.name} files.`);
            let downloadedFiles = [];
            for (const file of files) {
                if (ns.isRunning(file, 'home')) {
                    ns.scriptKill(file, 'home');
                }
                const url = `${pkg}/src/${file}`;
                const path = `${(file.includes('/') ? '/' + file : file)}`;
                ns.tprint(`INFO Downloading ${pkgInfo.name} => ${path}`);
                if (await ns.wget(url, path)) {
                    downloadedFiles.push(file);
                    let contents = ns.read(path);
                    contents = contents.replace(/from "(.*)"/g, 'from "/$1"');
                    await ns.write(path, contents, 'w');
                }
                else {
                    ns.tprint(`ERROR Unable to download ${pkgInfo.name} => ${path}`);
                }
            }
            if (files.length === downloadedFiles.length) {
                packages[pkgInfo.name] = {
                    name: pkgInfo.name,
                    description: pkgInfo.description,
                    version: pkgInfo.version,
                    url: pkg,
                    files: files
                };
                localStorage.bpmPackages = JSON.stringify(packages);
                ns.tprint(`INFO ${pkgInfo.name} Installed!`);
            }
            else {
                ns.tprint('ERROR Unable to download all OS files');
            }
        }
        else {
            ns.tprint(`ERROR Unable to download package.json file!`);
        }
    }
    else {
        ns.tprint(`ERROR Unable to download package.json file!`);
    }
}
export async function uninstall(ns, pkg) {
    if (ns.getHostname() !== "home") {
        throw new Error("Run the script from home");
    }
    let packages = {};
    if (localStorage.bpmPackages) {
        packages = JSON.parse(localStorage.bpmPackages);
    }
    if (packages[pkg]) {
        for (let file of packages[pkg].files) {
            if (ns.rm(`/${file}`)) {
                ns.tprint(`INFO Deleted ${pkg} => ${file}`);
            }
            else {
                ns.tprint(`ERROR Unable to delete ${pkg} => ${file}`);
            }
        }
        delete packages[pkg];
        localStorage.bpmPackages = JSON.stringify(packages);
    }
    else {
        ns.tprint(`ERROR ${pkg} not installed!`);
    }
}
export async function update(ns, pkg) {
    if (ns.getHostname() !== "home") {
        throw new Error("Run the script from home");
    }
    let packages = {};
    if (localStorage.bpmPackages) {
        packages = JSON.parse(localStorage.bpmPackages);
    }
    if (packages[pkg]) {
        await install(ns, packages[pkg].url);
    }
    else {
        ns.tprint(`ERROR ${pkg} not installed!`);
    }
}
export async function list(ns) {
    if (ns.getHostname() !== "home") {
        throw new Error("Run the script from home");
    }
    let packages = {};
    if (localStorage.bpmPackages) {
        packages = JSON.parse(localStorage.bpmPackages);
    }
    for (let name in packages) {
        const pkg = packages[name];
        ns.tprint(`INFO Package(${name}) Version(${pkg.version}) Description: ${pkg.description}`);
    }
}
export async function get(ns, pkg) {
    if (ns.getHostname() !== "home") {
        throw new Error("Run the script from home");
    }
    let packages = {};
    if (localStorage.bpmPackages) {
        packages = JSON.parse(localStorage.bpmPackages);
    }
    if (packages[pkg]) {
        ns.tprint(`INFO Package(${pkg}) Description: ${packages[pkg].description}`);
        ns.tprint(`INFO Package(${pkg}) URL:  ${packages[pkg].url}`);
        ns.tprint(`INFO Package(${pkg}) Version: ${packages[pkg].version}`);
        ns.tprint(`INFO Package(${pkg}) Files: ${JSON.stringify(packages[pkg].files)}`);
    }
    else {
        ns.tprint(`ERROR ${pkg} not installed!`);
    }
}
export async function updateOS(ns) {
    ns.tprint(`Updating BitBurnerOS`);
    const baseUrl = 'https://gitlab.com/bitburneros/base/-/raw/main/';
    const installerFiles = ['files.js', 'updateOS.js'];
    if (ns.getHostname() !== "home") {
        throw new Error("Run the script from home");
    }
    let downloadedFiles = [];
    for (let file of installerFiles) {
        if (ns.isRunning(file, 'home')) {
            ns.scriptKill(file, 'home');
        }
        ns.tprint(`INFO Downloading ${baseUrl}${file} => ${file}`);
        if (await ns.wget(`${baseUrl}${file}`, file)) {
            downloadedFiles.push(file);
        }
        else {
            ns.tprint(`ERROR Unable to download ${baseUrl}${file}`);
        }
    }
    if (installerFiles.length === downloadedFiles.length) {
        ns.tprint('Starting OS Updater');
        ns.spawn('updateOS.js', 1);
    }
    else {
        ns.tprint('ERROR Unable to download all install files!');
    }
}
