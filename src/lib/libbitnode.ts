import {BitNodeMultipliers, NS} from "../../index";
import {getPlayer} from "lib/libplayer";

export function getSourceFiles(): { [id: number]: number } | false {
  if (localStorage.sourceFiles) {
    return JSON.parse(localStorage.sourceFiles);
  } else {
    return false;
  }
}
export function getBitNodeMultipliers(): BitNodeMultipliers | false {
  if (localStorage.sourceFiles) {
    return JSON.parse(localStorage.sourceFiles);
  } else {
    return false;
  }
}
export function updateSourceFiles(ns: NS) {
  let sourceFiles = ns.getOwnedSourceFiles();
  let sourceFilesTree: { [id: number]: number } = {};
  sourceFiles.forEach((sourceFile) => {
    sourceFilesTree[sourceFile.n] = sourceFile.lvl;
  });
  const player = getPlayer();
  if (player && !sourceFilesTree[player.bitNodeN]) {
    sourceFilesTree[player.bitNodeN] = 0;
  }
  localStorage.sourceFiles = JSON.stringify(sourceFilesTree);

}
export function updateBitNodeMultipliers(ns: NS) {
  const sourceFiles = getSourceFiles();
  if (sourceFiles && sourceFiles[5]) {
    localStorage.bitNodeMultipliers = JSON.stringify(ns.getBitNodeMultipliers());
  }
}
