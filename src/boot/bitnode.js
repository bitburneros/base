import { getSourceFiles, updateBitNodeMultipliers, updateSourceFiles } from "lib/libbitnode";
const cmds = ['start', 'stop', 'status'];
export function autocomplete(data, args) {
    if (args.length === 0 || !cmds.includes(args[0])) {
        return cmds;
    }
    else {
        return [];
    }
}
export async function main(ns) {
    if (ns.args.length === 0 || ns.args[0] === 'start') {
        updateSourceFiles(ns);
        updateBitNodeMultipliers(ns);
    }
    else if (ns.args[0] === 'stop') {
        if (ns.isRunning('/boot/bitnode.js', 'home', 'start')) {
            ns.scriptKill('/boot/bitnode.js', 'home');
        }
    }
    else if (ns.args[0] === 'status') {
        ns.tprint(JSON.stringify(getSourceFiles(), null, 2));
    }
}
