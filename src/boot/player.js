import { getPlayer, updatePlayer } from "lib/libplayer";
const cmds = ['start', 'stop', 'status'];
export function autocomplete(data, args) {
    if (args.length === 0 || !cmds.includes(args[0])) {
        return cmds;
    }
    else {
        return [];
    }
}
export async function main(ns) {
    if (ns.getHostname() !== "home") {
        throw new Error("Run the script from home");
    }
    if (ns.args.length === 0 || ns.args[0] === 'start') {
        if (ns.isRunning('/boot/player.js', 'home')) {
            ns.scriptKill('/boot/player.js', 'home');
        }
        while (true) {
            updatePlayer(ns);
            await ns.sleep(100);
        }
    }
    else if (ns.args[0] === 'stop') {
        if (ns.isRunning('/boot/player.js', 'home', 'start')) {
            ns.scriptKill('/boot/player.js', 'home');
        }
    }
    else if (ns.args[0] === 'status') {
        ns.tprint(JSON.stringify(getPlayer(), null, 2));
    }
}
