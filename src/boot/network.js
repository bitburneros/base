import { getNetwork, mapNetwork } from "lib/libnetwork";
const cmds = ['start', 'stop', 'status'];
export function autocomplete(data, args) {
    if (args.length === 0 || !cmds.includes(args[0])) {
        return cmds;
    }
    else {
        return [];
    }
}
export async function main(ns) {
    if (ns.args.length === 0 || ns.args[0] === 'start') {
        while (true) {
            localStorage.network = JSON.stringify(await mapNetwork(ns, 'home', ''));
            localStorage.ownedServers = JSON.stringify(ns.getPurchasedServers());
            const maxRam = ns.getPurchasedServerMaxRam();
            let ramCosts = {};
            let ram = 0;
            for (let i = 1; ram < maxRam; i++) {
                ram = Math.pow(2, i);
                ramCosts[ram] = ns.getPurchasedServerCost(ram);
            }
            localStorage.ownedServerCosts = JSON.stringify(ramCosts);
            await ns.sleep(1000);
        }
    }
    else if (ns.args[0] === 'stop') {
        if (ns.isRunning('/boot/network.js', 'home', 'start')) {
            ns.scriptKill('/boot/network.js', 'home');
        }
    }
    else if (ns.args[0] === 'status') {
        ns.tprint(JSON.stringify(getNetwork(), null, 2));
    }
}
