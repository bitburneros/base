import { getHacknetNodes, updateHacknetNodes } from "lib/libhacknet";
const cmds = ['start', 'stop', 'status'];
export function autocomplete(data, args) {
    if (args.length === 0 || !cmds.includes(args[0])) {
        return cmds;
    }
    else {
        return [];
    }
}
export async function main(ns) {
    if (ns.getHostname() !== "home") {
        throw new Error("Run the script from home");
    }
    if (ns.args.length === 0 || ns.args[0] === 'start') {
        if (ns.isRunning('/boot/hacknet.js', 'home')) {
            ns.scriptKill('/boot/hacknet.js', 'home');
        }
        while (true) {
            updateHacknetNodes(ns);
            await ns.sleep(100);
        }
    }
    else if (ns.args[0] === 'stop') {
        if (ns.isRunning('/boot/hacknet.js', 'home', 'start')) {
            ns.scriptKill('/boot/hacknet.js', 'home');
        }
    }
    else if (ns.args[0] === 'status') {
        ns.tprint(JSON.stringify(getHacknetNodes(), null, 2));
    }
}
