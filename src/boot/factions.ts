import {AutocompleteData, NS} from "../../index";
import {getFactions, updateFactions} from "lib/libfactions";

const cmds = ['start', 'stop', 'status'];
export function autocomplete(data: AutocompleteData, args: string[]) {
  if (args.length === 0 || !cmds.includes(args[0])) {
    return cmds;
  }
  else {
    return [];
  }
}

export async function main(ns: NS) {
  if (ns.getHostname() !== "home") { throw new Error("Run the script from home"); }
  if (ns.args.length === 0 || ns.args[0] === 'start') {
    if (ns.isRunning('/boot/factions.js', 'home')) {
      ns.scriptKill('/boot/factions.js', 'home');
    }
    while (true) {
      updateFactions(ns);
      await ns.sleep(100);
    }
  }
  else if (ns.args[0] === 'stop') {
    if (ns.isRunning('/boot/factions.js', 'home', 'start')) {
      ns.scriptKill('/boot/factions.js', 'home');
    }
  }
  else if (ns.args[0] === 'status') {
    ns.tprint(JSON.stringify(getFactions(), null, 2));
  }
}
