import * as gulp from 'gulp';
import * as ts from 'gulp-typescript';
import * as glob from 'glob';
import * as fs from 'fs';

async function compileProject() {
  const tsProject = ts.createProject('tsconfig.json');
  return gulp.src(['src/**/*.+(ts|tsx)'])
    .pipe(tsProject())
    .pipe(gulp.dest('src'));
}
compileProject.displayName = 'compile:project';
compileProject.description = 'Compile the project';
gulp.task('Compile Project', compileProject);

async function compileInstaller() {
  const tsProject = ts.createProject('tsconfig.json');
  return gulp.src(['getOS.ts', 'installOS.ts', 'updateOS.ts', 'files.ts', 'OSFiles.ts'])
    .pipe(tsProject())
    .pipe(gulp.dest('.'));
}
compileInstaller.displayName = 'compile:installer';
compileInstaller.description = 'Compile the installer';
gulp.task('Compile Installer', compileInstaller);

async function buildFileList() {
  let out = [];
  let files = glob.sync('src/**/*.+(script|ns|js|txt)');
  for (let f in files) {
    out.push(files[f].substring(4))
  }
  fs.writeFileSync('files.ts', `export const files = ${JSON.stringify(out)};`);
  fs.writeFileSync('OSFiles.ts', `export const files = ${JSON.stringify(out)};`);
  return true;
}
buildFileList.displayName = 'build:files';
buildFileList.description = 'Build files list';
gulp.task('Build Files List', buildFileList);

gulp.task('default', gulp.series(compileProject, buildFileList, compileInstaller))
