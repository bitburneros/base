import {NS} from './index';
import {files} from "./files";

export async function main(ns: NS) {
  const baseUrl = 'https://gitlab.com/bitburneros/base/-/raw/main/src';
  if (ns.getHostname() !== "home") {
    throw new Error("Run the script from home");
  }

  let downloadedFiles = [];
  for (const file of files) {
    if (ns.isRunning(file, 'home')) {
      ns.scriptKill(file, 'home');
    }
    const url = `${baseUrl}/${file}`;
    const path = `${(file.includes('/')?'/'+file:file)}`;
    if (await ns.wget(url, path)) {
      downloadedFiles.push(file);
      let contents = ns.read(path);
      contents = contents.replace(/from "(.*)";/g, 'from "/$1";');
      await ns.write(path, contents, 'w');
      ns.tprint(`INFO Downloaded ${url} to ${path}`);
    } else {
      ns.tprint(`ERROR Unable to download ${url}`);
    }
  }
  if (files.length === downloadedFiles.length) {
    ns.tprint('BitBurnerOS Installed!');
    ns.mv('home', 'files.js', 'OSFiles.js');
  } else {
    ns.tprint('ERROR Unable to download all OS files');
  }
}
