import { files } from "./files";
import { files as oldFiles } from "./OSFiles";
export async function main(ns) {
    const baseUrl = 'https://gitlab.com/bitburneros/base/-/raw/main/src';
    if (ns.getHostname() !== "home") {
        throw new Error("Run the script from home");
    }
    oldFiles.forEach((oldFile) => {
        if (ns.isRunning(oldFile, 'home')) {
            ns.scriptKill(oldFile, 'home');
        }
        ns.rm(`/${oldFile}`, 'home');
    });
    let downloadedFiles = [];
    for (const file of files) {
        const url = `${baseUrl}/${file}`;
        const path = `${(file.includes('/') ? '/' + file : file)}`;
        if (await ns.wget(url, path)) {
            downloadedFiles.push(file);
            let contents = ns.read(path);
            contents = contents.replace(/from "(.*)";/g, 'from "/$1";');
            await ns.write(path, contents, 'w');
            ns.tprint(`INFO Downloaded ${url} to ${path}`);
        }
        else {
            ns.tprint(`ERROR Unable to download ${url}`);
        }
    }
    if (files.length === downloadedFiles.length) {
        ns.tprint('BitBurnerOS Updated!');
        ns.rm('OSFiles.js', 'home');
        ns.mv('home', 'files.js', 'OSFiles.js');
        ns.rm('updateOS.js', 'home');
    }
    else {
        ns.tprint('ERROR Unable to download all OS files');
    }
}
